#pragma once

#include <cstdint>
#include <vulkan/vulkan.h>
#include <vector>
#include "../vulkanUtils.h"
#include "../algorithm.h"


using Core::VulkanManager;
class FlyingEdges : public Algorithm
{

public:
    FlyingEdges(
            VulkanManager *VulkanManager,
            const int Dims[3],
            const std::vector<float> *Scalars);
    virtual ~FlyingEdges() override;

    virtual bool Init() override;
    virtual bool Run(
            const float Iso) override;
    virtual int NumVertices() const override;
    virtual int NumTriangles() const override;

private:
    VulkanManager *VkManager;
    const int XDim, YDim, ZDim;
    const int NumberOfEdges;

    const std::vector<float> *Scalars;
    VkBuffer InputBuf;
    VkDeviceMemory InputMem;

    struct UniformBufferObject
    {
        glm::ivec3 Dimension;
    };

    UniformBufferObject UBO;
    VkBuffer UBOBuf;
    VkDeviceMemory UBOMem;

    int32_t *XCases;
    VkBuffer XCasesBuf;
    VkDeviceMemory XCasesMem;

    int32_t *EdgeMetaData;
    VkBuffer EdgeMetaDataBuf;
    VkDeviceMemory EdgeMetaDataMem;

    VkBuffer EdgeCasesBuf;
    VkDeviceMemory EdgeCasesMem;
    VkBuffer EdgeUsesBuf;
    VkDeviceMemory EdgeUsesMem;

    VkBuffer ResultSizeBuf;
    VkDeviceMemory ResultSizeMem;

    VkBuffer IncludeAxesBuf;
    VkDeviceMemory IncludeAxesMem;

    VkPipeline Pipelines[4];
    VkPipelineLayout PipelineLayouts[4];
    VkDescriptorPool DescriptorPool;
    VkDescriptorSetLayout DescriptorSetLayouts[4];
    VkDescriptorSet DescriptorSets[4];

private:

    bool CreateDescriptorPool();
    bool CopyInputDataToDevice();
    bool CreateDescriptorSetLayouts();
    bool AllocateDescriptorSets();
    bool CreatePipelines();

// Pesistent data
    bool CreateUBO();
    bool CreatePersistentBuffers();
    void WritePass1DescriptorSet();
    void WritePass2DescriptorSet();
    void WritePass3DescriptorSet();
    void WritePass4DescriptorSet();

    void Pass1(
            const float Iso);
    void Pass2(
            const float Iso);
    bool Pass3();
    bool Pass4(
            const float Iso);
    bool PrepareVertexIndexBuffers();

    void FreeOutput();

// Tables
private:
    int EdgeCases[256][16];
    static const int VertMap[12][2];
    static const int VertOffsets[8][3];
    int EdgeUses[256][12];
    int IncludeAxes[256];

    void GenerateTables();

// Results
private:
    VkBuffer VertexBuf;
    VkDeviceMemory VertexMem;
    VkBuffer IndexBuf;
    VkDeviceMemory IndexMem;

    int NumXVerts;
    int NumYVerts;
    int NumZVerts;
    int NumTris;
};
