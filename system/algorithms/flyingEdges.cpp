// Flying Edges algorithm
// Parallel GPU implementation based on the original CPU implementation
// in the vtk library:
// https://gitlab.kitware.com/vtk/vtk/blob/master/Filters/Core/vtkFlyingEdges3D.cxx

#include "flyingEdges.h"
#include "../coreTypes.h"
#include "../globalSettings.h"
#include "../mathHelper.h"
#include "tables.h"
#include <SDL.h>
#include <glm/glm.hpp>
#include <string>
#include <sstream>



FlyingEdges::FlyingEdges(
        VulkanManager *VulkanManager,
        const int Dims[3],
        const std::vector<float> *Scalars) :
    VkManager(VulkanManager),
    XDim(Dims[0]),
    YDim(Dims[1]),
    ZDim(Dims[2]),
    NumberOfEdges(YDim * ZDim),
    Scalars(Scalars)
{
    EdgeMetaData = nullptr;
    XCases = nullptr;
    EdgeCasesBuf = VK_NULL_HANDLE;
    EdgeUsesBuf = VK_NULL_HANDLE;
    EdgeUsesMem = VK_NULL_HANDLE;
    EdgeUsesBuf = VK_NULL_HANDLE;
    VertexBuf = VK_NULL_HANDLE;
    VertexMem = VK_NULL_HANDLE;
    IndexBuf = VK_NULL_HANDLE;
    IndexMem = VK_NULL_HANDLE;
    ResultSizeBuf = VK_NULL_HANDLE;
    ResultSizeMem = VK_NULL_HANDLE;
    IncludeAxesBuf = VK_NULL_HANDLE;
    IncludeAxesMem = VK_NULL_HANDLE;

    NumXVerts = NumYVerts = NumZVerts = NumTris = 0;

    SDL_memset(EdgeCases,
               0,
               sizeof(EdgeCases));
    SDL_memset(EdgeUses,
               0,
               sizeof(EdgeUses));
    SDL_memset(IncludeAxes,
               0,
               sizeof(IncludeAxes));
}

FlyingEdges::~FlyingEdges()
{
    FreeOutput();

    if(IncludeAxesMem)
    {
        VkManager->FreeDeviceMemory(IncludeAxesMem);
    }
    if(IncludeAxesBuf)
    {
        VkManager->DestroyBuffer(IncludeAxesBuf);
    }
    if(ResultSizeMem)
    {
        VkManager->FreeDeviceMemory(ResultSizeMem);
    }
    if(ResultSizeBuf)
    {
        VkManager->DestroyBuffer(ResultSizeBuf);
    }

    if(DescriptorPool)
    {
        VkManager->DestroyDescriptorPool(DescriptorPool);
    }
    for(int i = 0; i < 4; ++i)
    {
        if(PipelineLayouts[i])
        {
            VkManager->DestroyPipelineLayout(PipelineLayouts[i]);
        }
        if(DescriptorSetLayouts[i])
        {
            VkManager->DestroyDescriptorSetLayout(DescriptorSetLayouts[i]);
        }
        if(Pipelines[i])
        {
            VkManager->DestroyPipeline(Pipelines[i]);
        }
    }

    if(EdgeUsesMem)
    {
        VkManager->FreeDeviceMemory(EdgeUsesMem);
    }
    if(EdgeUsesBuf)
    {
        VkManager->DestroyBuffer(EdgeUsesBuf);
    }
    if(EdgeCasesMem)
    {
        VkManager->FreeDeviceMemory(EdgeCasesMem);
    }
    if(EdgeCasesBuf)
    {
        VkManager->DestroyBuffer(EdgeCasesBuf);
    }
    if(EdgeMetaDataMem)
    {
        VkManager->FreeDeviceMemory(EdgeMetaDataMem);
    }
    if(EdgeMetaDataBuf)
    {
        VkManager->DestroyBuffer(EdgeMetaDataBuf);
    }
    if(EdgeMetaData)
    {
        SDL_free(EdgeMetaData);
    }

    if(XCasesMem)
    {
        VkManager->FreeDeviceMemory(XCasesMem);
    }
    if(XCasesBuf)
    {
        VkManager->DestroyBuffer(XCasesBuf);
    }
    if(XCases)
    {
        SDL_free(XCases);
    }

    if(InputMem)
    {
        VkManager->FreeDeviceMemory(InputMem);
    }
    if(InputBuf)
    {
        VkManager->DestroyBuffer(InputBuf);
    }

    if(UBOMem)
    {
        VkManager->FreeDeviceMemory(UBOMem);
    }
    if(UBOBuf)
    {
        VkManager->DestroyBuffer(UBOBuf);
    }

    DescriptorPool = VK_NULL_HANDLE;
    VkManager = nullptr;
}

bool FlyingEdges::Init()
{
    GenerateTables();
    if (!CreateDescriptorPool() ||
        !CopyInputDataToDevice() ||
        !CreatePipelines() ||
        !CreatePersistentBuffers())
    {
        return false;
    }
    return true;
}

bool FlyingEdges::Run(
        const float Iso)
{
    uint64_t p1,p2,p3,p4, update;

    p1 = SDL_GetPerformanceCounter();
    Pass1(Iso);
    p1 = SDL_GetPerformanceCounter() - p1;

    p2 = SDL_GetPerformanceCounter();
    Pass2(Iso);
    p2 = SDL_GetPerformanceCounter() - p2;

    p3 = SDL_GetPerformanceCounter();
    if (!Pass3())
    {
        return false;
    }
    p3 = SDL_GetPerformanceCounter() - p3;

    p4 = SDL_GetPerformanceCounter();
    if (!Pass4(Iso))
    {
        return false;
    }
    p4 = SDL_GetPerformanceCounter() - p4;

    update = SDL_GetPerformanceCounter();
    if (NumTris > 0)
    {
        if (!VkManager->UpdateRenderCommandBuffers(
                    &VertexBuf,
                    &IndexBuf,
                    NumTris))
        {
            SDL_Log(
                    "UpdateRenderCommandBuffers() failed with:\n%s",
                    SDL_GetError());
            return false;
        }
    }
    update = SDL_GetPerformanceCounter() - update;


    static const uint64_t perfFreq = SDL_GetPerformanceFrequency();
    SDL_Log(
                "\n\nIso:\t\t%f\n"
                "-------------------------------\n"
                "Output:\n"
                "Vertices:\t\t%i\n"
                "Triangles:\t%i\n"
                "-------------------------------\n"
                "Timings (ms):\n"
                "Pass 1:\t\t%lf\n"
                "Pass 2:\t\t%lf\n"
                "Pass 3:\t\t%lf\n"
                "Pass 4:\t\t%lf\n"
                "Update:\t\t%lf\n"
                "Total:\t\t%lf\n"
                "-------------------------------\n"
                "-------------------------------\n\n",
                Iso,
                NumVertices(),
                NumTriangles(),
                p1 * 1000.0 / perfFreq,
                p2 * 1000.0 / perfFreq,
                p3 * 1000.0 / perfFreq,
                p4 * 1000.0 / perfFreq,
                update * 1000.0 / perfFreq,
                (p1 + p2 + p3 + p4 + update) * 1000.0 / perfFreq);

    return true;
}

int FlyingEdges::NumVertices() const
{
    return NumXVerts + NumYVerts + NumZVerts;
}

int FlyingEdges::NumTriangles() const
{
    return NumTris;
}

bool FlyingEdges::CreateUBO()
{
    UBO.Dimension = glm::ivec3(
                XDim,
                YDim,
                ZDim);

    VkDeviceSize bufferSize =
            sizeof(UniformBufferObject);
    if (!VkManager->CopyDataToDevice(
                bufferSize,
                (void*)&UBO,
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                UBOBuf,
                UBOMem))
    {
        SDL_SetError(
                    "Couldn't create UBO:\n%s",
                    SDL_GetError());
        return false;
    }

    VkWriteDescriptorSet descriptorWrites[4];
    const VkDescriptorBufferInfo uboInfo =
    {
        UBOBuf,
        0,
        VK_WHOLE_SIZE
    };
    for (int i = 0; i < 4; ++i)
    {
        descriptorWrites[i] =
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[i],
            0,
            0,
            1,
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            VK_NULL_HANDLE,
            &uboInfo,
            VK_NULL_HANDLE
        };
    }

    VkManager->UpdateDescriptorSets(
                descriptorWrites,
                4);

    return true;
}

bool FlyingEdges::CreateDescriptorPool()
{
    // Combined number of buffers for each pass
    std::array<VkDescriptorPoolSize, 2> descriptorTypes =
    {
        VkDescriptorPoolSize
        {
            VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            4
        },
        VkDescriptorPoolSize
        {
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            17
        },
    };

    VkDescriptorPoolCreateInfo poolInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        nullptr,
        0,
        4,
        (uint32_t)descriptorTypes.size(),
        descriptorTypes.data()
    };

    VkResult res = VkManager->CreateDescriptorPool(
                poolInfo,
                DescriptorPool);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create descriptor pool (%i)",
                    res);
        return false;
    }

    return true;
}

bool FlyingEdges::CopyInputDataToDevice()
{
    const int size =
            XDim * NumberOfEdges;
    VkDeviceSize bufferSize =
            sizeof(float) * size;
    if(!VkManager->CopyDataToDevice(
                bufferSize,
                (void*)Scalars->data(),
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                InputBuf,
                InputMem))
    {
        SDL_SetError(
                    "Couldn't copy input data to device\n%s",
                    SDL_GetError());
        return false;
    }

    return true;
}

bool FlyingEdges::CreateDescriptorSetLayouts()
{
    // One set per pass
    std::array<std::vector<VkDescriptorSetLayoutBinding>, 4> passBindings =
    {
        std::vector<VkDescriptorSetLayoutBinding>
        {
            // UBO
            VkDescriptorSetLayoutBinding
            {
                0,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Input data
            VkDescriptorSetLayoutBinding
            {
                1,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // XCases
            VkDescriptorSetLayoutBinding
            {
                2,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Edge Meta
            VkDescriptorSetLayoutBinding
            {
                3,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            }
        },

        std::vector<VkDescriptorSetLayoutBinding>
        {
            // UBO
            VkDescriptorSetLayoutBinding
            {
                0,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // XCases
            VkDescriptorSetLayoutBinding
            {
                1,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // EdgeMeta
            VkDescriptorSetLayoutBinding
            {
                2,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // EdgeCases
            VkDescriptorSetLayoutBinding
            {
                3,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // EdgeUses
            VkDescriptorSetLayoutBinding
            {
                4,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            }
        },

        std::vector<VkDescriptorSetLayoutBinding>
        {
            // UBO
            VkDescriptorSetLayoutBinding
            {
                0,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // EdgeMeta
            VkDescriptorSetLayoutBinding
            {
                1,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Result size
            VkDescriptorSetLayoutBinding
            {
                2,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            }
        },

        std::vector<VkDescriptorSetLayoutBinding>
        {
            // UBO
            VkDescriptorSetLayoutBinding
            {
                0,
                VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Input data
            VkDescriptorSetLayoutBinding
            {
                1,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Edge meta
            VkDescriptorSetLayoutBinding
            {
                2,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Edge uses
            VkDescriptorSetLayoutBinding
            {
                3,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // X edge cases
            VkDescriptorSetLayoutBinding
            {
                4,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Edge cases
            VkDescriptorSetLayoutBinding
            {
                5,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Include axes
            VkDescriptorSetLayoutBinding
            {
                6,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Vertex buffer
            VkDescriptorSetLayoutBinding
            {
                7,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            },
            // Index buffer
            VkDescriptorSetLayoutBinding
            {
                8,
                VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                1,
                VK_SHADER_STAGE_COMPUTE_BIT,
                nullptr
            }
        },
    };

    for (int i = 0; i < 4; ++i)
    {
        VkDescriptorSetLayoutCreateInfo layoutInfo =
        {
            VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            nullptr,
            0,
            (uint32_t)passBindings[i].size(),
            passBindings[i].data()
        };

        VkResult res = VkManager->CreateDescriptorSetLayout(
                    layoutInfo,
                    DescriptorSetLayouts[i]);
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't create descriptor set layout #%i (%i)",
                        i,
                        res);
            return false;
        }
    }

    return true;
}

bool FlyingEdges::AllocateDescriptorSets()
{
    VkDescriptorSetAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        nullptr,
        DescriptorPool,
        4,
        DescriptorSetLayouts
    };

    VkResult res = VkManager->AllocateDesctriptorSets(
                allocInfo,
                *DescriptorSets);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't allocate descriptor sets (%i)",
                    res);
        return false;
    }

    return true;
}

bool FlyingEdges::CreatePipelines()
{
    if (!CreateDescriptorSetLayouts() ||
        !AllocateDescriptorSets() ||
        !CreateUBO())
    {
        return false;
    }

    std::string path =
            "shaders/FE/comp_pass1.spv";

    // One for each pass
    const VkPushConstantRange pushConstantRanges[4] =
    {
        VkPushConstantRange
        {
            VK_SHADER_STAGE_COMPUTE_BIT,
            0,
            4
        },
        VkPushConstantRange
        {
            VK_SHADER_STAGE_COMPUTE_BIT,
            0,
            4
        },
        VkPushConstantRange
        {
            VK_SHADER_STAGE_COMPUTE_BIT,
            0,
            4
        },
        VkPushConstantRange
        {
            VK_SHADER_STAGE_COMPUTE_BIT,
            0,
            4
        }
    };
    VkComputePipelineCreateInfo pipelineInfos[4];
    VkShaderModule *shaderModules =
            (VkShaderModule*)SDL_malloc(sizeof(VkShaderModule) * 4);

    // Passes are named ..._passN.spv
    const unsigned int idx = path.length() - 5;
    for(int i = 0; i < 4; ++i)
    {
        path[idx] = (char)(i + 49);
        std::vector<char> shader;
        if(!VkManager->LoadShaderFromDisk(
                    shader,
                    path.data()))
        {
            SDL_SetError(
                        "Couldn't load compute shader for pass %i "
                        "from disk:\n%s",
                        i,
                        SDL_GetError());
            return false;
        }
        if(!VkManager->CreateShaderModule(
                    shader,
                    shaderModules[i]))
        {
            SDL_SetError(
                        "Couldn't create compute shader module for "
                        "pass %i:\n%s",
                        i,
                        SDL_GetError());
            return false;
        }
        shader.empty();

        VkPipelineShaderStageCreateInfo shaderStageInfo =
        {
            VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            nullptr,
            0,
            VK_SHADER_STAGE_COMPUTE_BIT,
            shaderModules[i],
            "main",
            VK_NULL_HANDLE
        };

        VkPipelineLayoutCreateInfo layoutInfo =
        {
            VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            nullptr,
            0,
            1,
            &DescriptorSetLayouts[i],
            1,
            &pushConstantRanges[i]
        };

        VkResult res = VkManager->CreatePipelineLayout(
                    layoutInfo,
                    PipelineLayouts[i]);
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't create compute pipeline layout (%i)",
                        res);
            return false;
        }

        pipelineInfos[i] = VkComputePipelineCreateInfo
        {
            VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
            nullptr,
            0,
            shaderStageInfo,
            PipelineLayouts[i],
            VK_NULL_HANDLE,
            -1
        };
    }

    VkResult res = VkManager->CreateComputePipelines(
                pipelineInfos,
                4,
                *Pipelines);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create compute pipelines (%i)",
                    res);
        return false;
    }

    for(int i = 0; i < 4; ++i)
    {
        VkManager->DestroyShaderModule(shaderModules[i]);
    }
    SDL_free(shaderModules);

    return true;
}

bool FlyingEdges::CreatePersistentBuffers()
{
    VkDeviceSize bufferSize =
            sizeof(int32_t) * (XDim - 1) * NumberOfEdges;
    if(!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                XCasesBuf,
                XCasesMem))
    {
        SDL_SetError(
                    "Couldn't create pass 1 buffers\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(int32_t) * NumberOfEdges * 6;
    if(!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                EdgeMetaDataBuf,
                EdgeMetaDataMem))
    {
        SDL_SetError(
                    "Couldn't create pass 1 buffers\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(EdgeCases);
    if (!VkManager->CopyDataToDevice(
                bufferSize,
                EdgeCases,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                EdgeCasesBuf,
                EdgeCasesMem))
    {
        SDL_SetError(
                    "Couldn't allocate pass 2 buffers:\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize = sizeof(EdgeUses);
    if(!VkManager->CopyDataToDevice(
                bufferSize,
                EdgeUses,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                EdgeUsesBuf,
                EdgeUsesMem))
    {
        SDL_SetError(
                    "Couldn't allocate pass 2 buffers:\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(int32_t) * 4;
    if (!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT |
                VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
                ResultSizeBuf,
                ResultSizeMem))
    {
        SDL_SetError(
                    "CreateBuffer(pass 3) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(IncludeAxes);
    if (!VkManager->CopyDataToDevice(
                bufferSize,
                IncludeAxes,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                IncludeAxesBuf,
                IncludeAxesMem))
    {
        SDL_SetError(
                    "CopyDataToDevice(include axes) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    WritePass1DescriptorSet();
    WritePass2DescriptorSet();
    WritePass3DescriptorSet();
    WritePass4DescriptorSet();

    return true;
}

void FlyingEdges::Pass1(
        const float Iso)
{
    VkCommandBuffer cmd =
            VkManager->BeginSingleTimeCommands();

    vkCmdBindPipeline(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                Pipelines[0]);

    vkCmdBindDescriptorSets(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                PipelineLayouts[0],
                0,
                1,
                &DescriptorSets[0],
                0,
                0);

    vkCmdPushConstants(
                cmd,
                PipelineLayouts[0],
                VK_SHADER_STAGE_COMPUTE_BIT,
                0,
                sizeof(float),
                (void*)&Iso);

    vkCmdDispatch(
                cmd,
                1,
                YDim,
                ZDim);

    VkManager->EndSingleTimeCommands(
                cmd,
                VkManager->ComputeQueue());
}

void FlyingEdges::WritePass1DescriptorSet()
{
    VkDescriptorBufferInfo inputInfo =
    {
        InputBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo xCaseInfo =
    {
        XCasesBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo metaInfo =
    {
        EdgeMetaDataBuf,
        0,
        VK_WHOLE_SIZE
    };

    std::array<VkWriteDescriptorSet, 3> descriptorWrites =
    {
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[0],
            1,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &inputInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[0],
            2,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &xCaseInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[0],
            3,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &metaInfo,
            VK_NULL_HANDLE
        }
    };

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());
}

void FlyingEdges::Pass2(
        const float Iso)
{
    VkCommandBuffer cmd =
            VkManager->BeginSingleTimeCommands();

    vkCmdBindPipeline(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                Pipelines[1]);

    vkCmdBindDescriptorSets(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                PipelineLayouts[1],
                0,
                1,
                &DescriptorSets[1],
                0,
                0);

    vkCmdPushConstants(
                cmd,
                PipelineLayouts[1],
                VK_SHADER_STAGE_COMPUTE_BIT,
                0,
                sizeof(float),
                (void*)&Iso);

    vkCmdDispatch(
                cmd,
                1,
                YDim - 1,
                ZDim - 1);

    VkManager->EndSingleTimeCommands(
                cmd,
                VkManager->ComputeQueue());
}

void FlyingEdges::WritePass2DescriptorSet()
{
    VkDescriptorBufferInfo xCaseInfo =
    {
        XCasesBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo metaInfo =
    {
        EdgeMetaDataBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo casesInfo =
    {
        EdgeCasesBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo usesInfo =
    {
        EdgeUsesBuf,
        0,
        VK_WHOLE_SIZE
    };

    std::array<VkWriteDescriptorSet, 4> descriptorWrites =
    {
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[1],
            1,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &xCaseInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[1],
            2,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &metaInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[1],
            3,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &casesInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[1],
            4,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &usesInfo,
            VK_NULL_HANDLE
        }
    };

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());
}

bool FlyingEdges::Pass3()
{
    VkCommandBuffer cmd =
            VkManager->BeginSingleTimeCommands();

    vkCmdBindPipeline(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                Pipelines[2]);

    vkCmdBindDescriptorSets(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                PipelineLayouts[2],
                0,
                1,
                &DescriptorSets[2],
                0,
                0);

    vkCmdDispatch(
                cmd,
                1,
                1,
                1);

    VkManager->EndSingleTimeCommands(
                cmd,
                VkManager->ComputeQueue());

    VkDeviceSize bufferSize =
            sizeof(int32_t) * 4;
    void *data = nullptr;
    if (!VkManager->CopyDataToHostDirect(
                bufferSize,
                ResultSizeMem,
                data))
    {
        SDL_SetError(
                    "CopyDataToHostDirect(pass 3) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    int32_t *results = (int32_t*)data;
    NumXVerts = results[0];
    NumYVerts = results[1];
    NumZVerts = results[2];
    NumTris = results[3];

    SDL_free(data);

    return true;
}

void FlyingEdges::WritePass3DescriptorSet()
{
    VkDescriptorBufferInfo metaInfo =
    {
        EdgeMetaDataBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo resultInfo =
    {
        ResultSizeBuf,
        0,
        VK_WHOLE_SIZE
    };

    std::array<VkWriteDescriptorSet, 2> descriptorWrites =
    {
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[2],
            1,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &metaInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[2],
            2,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &resultInfo,
            VK_NULL_HANDLE
        }
    };

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());
}

bool FlyingEdges::Pass4(
        const float Iso)
{
    if (NumTris > 0)
    {
        if (!PrepareVertexIndexBuffers())
        {
            return false;
        }
        WritePass4DescriptorSet();

        VkCommandBuffer cmd =
                VkManager->BeginSingleTimeCommands();

        vkCmdBindPipeline(
                    cmd,
                    VK_PIPELINE_BIND_POINT_COMPUTE,
                    Pipelines[3]);

        vkCmdBindDescriptorSets(
                    cmd,
                    VK_PIPELINE_BIND_POINT_COMPUTE,
                    PipelineLayouts[3],
                    0,
                    1,
                    &DescriptorSets[3],
                    0,
                    0);

        vkCmdPushConstants(
                    cmd,
                    PipelineLayouts[3],
                    VK_SHADER_STAGE_COMPUTE_BIT,
                    0,
                    sizeof(float),
                    (void*)&Iso);

        vkCmdDispatch(
                    cmd,
                    2,
                    YDim - 1,
                    ZDim - 1);

        VkManager->EndSingleTimeCommands(
                    cmd,
                    VkManager->ComputeQueue());
    }

    return true;
}

bool FlyingEdges::PrepareVertexIndexBuffers()
{
    FreeOutput();
    const int numPoints =
            NumXVerts + NumYVerts + NumZVerts;

    VkDeviceSize bufferSize =
            numPoints * sizeof(Model::Vertex);
    if(!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                VertexBuf,
                VertexMem))
    {
        SDL_SetError(
                    "CreateBuffer(vertex) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            NumTris * sizeof(int32_t) * 3;
    if (!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                IndexBuf,
                IndexMem))
    {
        SDL_SetError(
                    "CreateBuffer(index) failed with:\n%s",
                    SDL_GetError());
        return false;
    }


    std::array<VkDescriptorBufferInfo, 2> infos
    {
        VkDescriptorBufferInfo
        {
            VertexBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            IndexBuf,
            0,
            VK_WHOLE_SIZE
        }
    };

    std::vector<VkWriteDescriptorSet> descriptorWrites(infos.size());

    for (uint32_t i=0; i<infos.size(); ++i)
    {
        const VkWriteDescriptorSet write =
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[3],
            7 + i,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &infos[i],
            VK_NULL_HANDLE
        };

        descriptorWrites[i] = write;
    }

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());

    return true;
}

void FlyingEdges::WritePass4DescriptorSet()
{
    std::array<VkDescriptorBufferInfo, 6> infos
    {
        VkDescriptorBufferInfo
        {
            InputBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            EdgeMetaDataBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            EdgeUsesBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            XCasesBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            EdgeCasesBuf,
            0,
            VK_WHOLE_SIZE
        },
        VkDescriptorBufferInfo
        {
            IncludeAxesBuf,
            0,
            VK_WHOLE_SIZE
        },
    };

    std::vector<VkWriteDescriptorSet> descriptorWrites(infos.size());

    for (uint32_t i=0; i<infos.size(); ++i)
    {
        const VkWriteDescriptorSet write =
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSets[3],
            1 + i,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &infos[i],
            VK_NULL_HANDLE
        };

        descriptorWrites[i] = write;
    }

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());
}

void FlyingEdges::FreeOutput()
{
    if(IndexMem)
    {
        VkManager->FreeDeviceMemory(IndexMem);
        IndexMem = VK_NULL_HANDLE;
    }
    if(IndexBuf)
    {
        VkManager->DestroyBuffer(IndexBuf);
        IndexBuf = VK_NULL_HANDLE;
    }
    if(VertexMem)
    {
        VkManager->FreeDeviceMemory(VertexMem);
        VertexMem = VK_NULL_HANDLE;
    }
    if(VertexBuf)
    {
        VkManager->DestroyBuffer(VertexBuf);
        VertexBuf = VK_NULL_HANDLE;
    }
}

void FlyingEdges::GenerateTables()
{
    static int EdgeMap[12] =
    {
        0,
        5,
        1,
        4,
        2,
        7,
        3,
        6,
        8,
        9,
        11,
        10
    };
    static int vertMap[8] =
    {
        0,
        1,
        3,
        2,
        4,
        5,
        7,
        6
    };
    static int CASE_MASK[8] =
    {
        1,
        2,
        4,
        8,
        16,
        32,
        64,
        128
    };


    auto& triTable = MCConstants::TriTable;
    for (int i = 0; i < 256; ++i)
    {
        int index = 0;
        for (int ii = 0; ii < 8; ++ii)
        {
            if (i & (1 << vertMap[ii]))
            {
                index |= CASE_MASK[ii];
            }
        }

        int numTris = 0;
        int *edge = triTable[index];
        for (; edge[0] > -1; edge += 3)
        {
            ++numTris;
        }

        if(numTris > 0)
        {
            int *edgeCase = EdgeCases[i];
            *edgeCase++ = numTris;
            edge = triTable[index];
            for(; edge[0] > -1; edge += 3, edgeCase += 3)
            {
                edgeCase[0] = EdgeMap[edge[0]];
                edgeCase[1] = EdgeMap[edge[1]];
                edgeCase[2] = EdgeMap[edge[2]];
            }
        }
    }

    for (int eCase = 0; eCase < 256; ++eCase)
    {
        int *edgeCase = EdgeCases[eCase];
        int numTris = *edgeCase++;

        for (int i = 0; i < numTris * 3; ++i)
        {
            EdgeUses[eCase][edgeCase[i]] = 1;
        }

        IncludeAxes[eCase] =
                EdgeUses[eCase][0] |
                EdgeUses[eCase][4] |
                EdgeUses[eCase][8];
    }
}
