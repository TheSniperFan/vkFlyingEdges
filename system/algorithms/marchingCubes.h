#pragma once

#include <cstdint>
#include <vulkan/vulkan.h>
#include <vector>
#include "../vulkanUtils.h"
#include "../algorithm.h"


using Core::VulkanManager;
class MarchingCubes : public Algorithm
{

public:
    MarchingCubes(
            VulkanManager *VulkanManager,
            const int Dims[3],
            const std::vector<float> *Scalars);
    virtual ~MarchingCubes();

    virtual bool Init() override;
    virtual bool Run(
            const float Iso) override;
    virtual int NumVertices() const override;
    virtual int NumTriangles() const override;

private:
    VulkanManager *VkManager;
    const int XDim, YDim, ZDim;

    const std::vector<float> *Scalars;
    VkBuffer InputBuf;
    VkDeviceMemory InputMem;

    VkBuffer EdgeTableBuf;
    VkDeviceMemory EdgeTableMem;

    VkBuffer TriTableBuf;
    VkDeviceMemory TriTableMem;

    VkPipeline Pipeline;
    VkPipelineLayout PipelineLayout;
    VkDescriptorPool DescriptorPool;
    VkDescriptorSetLayout DescriptorSetLayout;
    VkDescriptorSet DescriptorSet;

private:
    bool CreateDescriptorPool();
    bool CopyInputDataToDevice();
    bool CreateDescriptorSetLayouts();
    bool AllocateDescriptorSets();
    bool CreatePipelines();

// Pesistent data
    bool CreatePersistentBuffers();
    void WriteDescriptorSet();

// Results
private:
    VkBuffer VertexBuf;
    VkDeviceMemory VertexMem;
    int NumVerts;
    int NumTris;
};
