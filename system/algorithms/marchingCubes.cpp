// Marching Cubes algorithm
// Based on the CPU implementation of the Marching Cubes Algorithm by
// Paul Bourke, http://paulbourke.net/geometry/polygonise/, 1994

#include "marchingCubes.h"
#include "../coreTypes.h"
#include "../globalSettings.h"
#include "../mathHelper.h"
#include "tables.h"
#include <SDL.h>
#include <glm/glm.hpp>



MarchingCubes::MarchingCubes(
        VulkanManager *VulkanManager,
        const int Dims[3],
        const std::vector<float> *Scalars) :
    VkManager(VulkanManager),
    XDim(Dims[0]),
    YDim(Dims[1]),
    ZDim(Dims[2]),
    Scalars(Scalars)
{
    InputBuf = VK_NULL_HANDLE;
    InputMem = VK_NULL_HANDLE;
    VertexBuf = VK_NULL_HANDLE;
    VertexMem = VK_NULL_HANDLE;
    EdgeTableBuf = VK_NULL_HANDLE;
    EdgeTableMem = VK_NULL_HANDLE;
    TriTableBuf = VK_NULL_HANDLE;
    TriTableMem = VK_NULL_HANDLE;
    Pipeline = VK_NULL_HANDLE;
    PipelineLayout = VK_NULL_HANDLE;
    DescriptorPool = VK_NULL_HANDLE;
    DescriptorSetLayout = VK_NULL_HANDLE;
    DescriptorSet = VK_NULL_HANDLE;

    NumVerts = 3 * (NumTris = XDim * YDim * ZDim * 5);
}

MarchingCubes::~MarchingCubes()
{
    if(TriTableMem)
    {
        VkManager->FreeDeviceMemory(TriTableMem);
        TriTableMem = VK_NULL_HANDLE;
    }
    if(TriTableBuf)
    {
        VkManager->DestroyBuffer(TriTableBuf);
        TriTableBuf = VK_NULL_HANDLE;
    }
    if(EdgeTableMem)
    {
        VkManager->FreeDeviceMemory(EdgeTableMem);
        EdgeTableMem = VK_NULL_HANDLE;
    }
    if(EdgeTableBuf)
    {
        VkManager->DestroyBuffer(EdgeTableBuf);
        EdgeTableBuf = VK_NULL_HANDLE;
    }
    if(VertexMem)
    {
        VkManager->FreeDeviceMemory(VertexMem);
        VertexMem = VK_NULL_HANDLE;
    }
    if(VertexBuf)
    {
        VkManager->DestroyBuffer(VertexBuf);
        VertexBuf = VK_NULL_HANDLE;
    }
    if(DescriptorPool)
    {
        VkManager->DestroyDescriptorPool(DescriptorPool);
    }
    if(PipelineLayout)
    {
        VkManager->DestroyPipelineLayout(PipelineLayout);
    }
    if(DescriptorSetLayout)
    {
        VkManager->DestroyDescriptorSetLayout(DescriptorSetLayout);
    }
    if(Pipeline)
    {
        VkManager->DestroyPipeline(Pipeline);
    }
    if(InputMem)
    {
        VkManager->FreeDeviceMemory(InputMem);
    }
    if(InputBuf)
    {
        VkManager->DestroyBuffer(InputBuf);
    }
}

bool MarchingCubes::Init()
{
    if (!CreateDescriptorPool() ||
        !CopyInputDataToDevice() ||
        !CreatePipelines() ||
        !CreatePersistentBuffers() ||
        !VkManager->UpdateRenderCommandBuffers(
                &VertexBuf,
                NumVertices()))
    {
        return false;
    }

    return true;
}

bool MarchingCubes::Run(
        const float Iso)
{
    uint64_t perf = SDL_GetPerformanceCounter();
    VkCommandBuffer cmd =
            VkManager->BeginSingleTimeCommands();

    vkCmdBindPipeline(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                Pipeline);

    vkCmdBindDescriptorSets(
                cmd,
                VK_PIPELINE_BIND_POINT_COMPUTE,
                PipelineLayout,
                0,
                1,
                &DescriptorSet,
                0,
                0);

    vkCmdPushConstants(
                cmd,
                PipelineLayout,
                VK_SHADER_STAGE_COMPUTE_BIT,
                0,
                sizeof(float),
                (void*)&Iso);

    vkCmdDispatch(
                cmd,
                XDim,
                YDim,
                ZDim);

    VkManager->EndSingleTimeCommands(
                cmd,
                VkManager->ComputeQueue());
    perf = SDL_GetPerformanceCounter() - perf;


    static const uint64_t perfFreq = SDL_GetPerformanceFrequency();
    SDL_Log(
                "\n\nIso:\t\t%f\n"
                "-------------------------------\n"
                "Output:\n"
                "Vertices:\t\t%i\n"
                "Triangles:\t%i\n"
                "-------------------------------\n"
                "Timings (ms):\n"
                "Update:\t\t%lf\n"
                "-------------------------------\n"
                "-------------------------------\n\n",
                Iso,
                NumVertices(),
                NumTriangles(),
                perf * 1000.0 / perfFreq);

    return true;
}

int MarchingCubes::NumVertices() const
{
    return NumVerts;
}

int MarchingCubes::NumTriangles() const
{
    return NumTris;
}

bool MarchingCubes::CreateDescriptorPool()
{
    VkDescriptorPoolSize descriptorType =
    {
        VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        4
    };

    VkDescriptorPoolCreateInfo poolInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        nullptr,
        0,
        4,
        1,
        &descriptorType
    };

    VkResult res = VkManager->CreateDescriptorPool(
                poolInfo,
                DescriptorPool);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create descriptor pool (%i)",
                    res);
        return false;
    }

    return true;
}

bool MarchingCubes::CopyInputDataToDevice()
{
    VkDeviceSize bufferSize =
            sizeof(float) * XDim * YDim * ZDim;
    if(!VkManager->CopyDataToDevice(
                bufferSize,
                (void*)Scalars->data(),
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                InputBuf,
                InputMem))
    {
        SDL_SetError(
                    "Couldn't copy input data to device\n%s",
                    SDL_GetError());
        return false;
    }

    return true;
}

bool MarchingCubes::CreateDescriptorSetLayouts()
{
    std::array<VkDescriptorSetLayoutBinding, 4> bindings =
    {
        // Input data
        VkDescriptorSetLayoutBinding
        {
            0,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            1,
            VK_SHADER_STAGE_COMPUTE_BIT,
            nullptr
        },
        // Output data
        VkDescriptorSetLayoutBinding
        {
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            1,
            VK_SHADER_STAGE_COMPUTE_BIT,
            nullptr
        },
        // Edge Table
        VkDescriptorSetLayoutBinding
        {
            2,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            1,
            VK_SHADER_STAGE_COMPUTE_BIT,
            nullptr
        },
        // Tri Table
        VkDescriptorSetLayoutBinding
        {
            3,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            1,
            VK_SHADER_STAGE_COMPUTE_BIT,
            nullptr
        }
    };

    VkDescriptorSetLayoutCreateInfo layoutInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        nullptr,
        0,
        (uint32_t)bindings.size(),
        bindings.data()
    };

    VkResult res = VkManager->CreateDescriptorSetLayout(
                layoutInfo,
                DescriptorSetLayout);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create descriptor set layout (%i)",
                    res);
        return false;
    }

    return true;
}

bool MarchingCubes::AllocateDescriptorSets()
{
    VkDescriptorSetAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        nullptr,
        DescriptorPool,
        1,
        &DescriptorSetLayout
    };

    VkResult res = VkManager->AllocateDesctriptorSets(
                allocInfo,
                DescriptorSet);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't allocate descriptor set (%i)",
                    res);
        return false;
    }

    return true;
}

bool MarchingCubes::CreatePipelines()
{
    if (!CreateDescriptorSetLayouts() ||
        !AllocateDescriptorSets())
    {
        return false;
    }

    const char *path =
            "shaders/MC/comp_marching.spv";

    const VkPushConstantRange pushConstantRange =
    {
        VK_SHADER_STAGE_COMPUTE_BIT,
        0,
        4
    };


    VkShaderModule shaderModule = VK_NULL_HANDLE;
    std::vector<char> shader;
    if(!VkManager->LoadShaderFromDisk(
                shader,
                path))
    {
        SDL_SetError(
                    "Couldn't load compute shader from disk:\n%s",
                    SDL_GetError());
        return false;
    }
    if(!VkManager->CreateShaderModule(
                shader,
                shaderModule))
    {
        SDL_SetError(
                    "Couldn't create compute shader module:\n%s",
                    SDL_GetError());
        return false;
    }
    shader.empty();


    VkPipelineShaderStageCreateInfo shaderStageInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,
        0,
        VK_SHADER_STAGE_COMPUTE_BIT,
        shaderModule,
        "main",
        VK_NULL_HANDLE
    };

    VkPipelineLayoutCreateInfo layoutInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        nullptr,
        0,
        1,
        &DescriptorSetLayout,
        1,
        &pushConstantRange
    };

    VkResult res = VkManager->CreatePipelineLayout(
                layoutInfo,
                PipelineLayout);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create compute pipeline layout (%i)",
                    res);
        return false;
    }

    VkComputePipelineCreateInfo pipelineInfo =
    {
        VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        nullptr,
        0,
        shaderStageInfo,
        PipelineLayout,
        VK_NULL_HANDLE,
        -1
    };

    res = VkManager->CreateComputePipelines(
                &pipelineInfo,
                1,
                Pipeline);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create compute pipeline (%i)",
                    res);
        return false;
    }

    VkManager->DestroyShaderModule(shaderModule);

    return true;
}

bool MarchingCubes::CreatePersistentBuffers()
{
    VkDeviceSize bufferSize =
            sizeof(Model::Vertex) * NumVerts;
    if(!VkManager->CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
                VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                VertexBuf,
                VertexMem))
    {
        SDL_SetError(
                    "Couldn't create vertex buffer\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(MCConstants::EdgeTable);
    if(!VkManager->CopyDataToDevice(
                bufferSize,
                (void*)MCConstants::EdgeTable,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                EdgeTableBuf,
                EdgeTableMem))
    {
        SDL_SetError(
                    "Couldn't copy edge table to device\n%s",
                    SDL_GetError());
        return false;
    }

    bufferSize =
            sizeof(MCConstants::TriTable);
    if(!VkManager->CopyDataToDevice(
                bufferSize,
                (void*)MCConstants::TriTable,
                VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                TriTableBuf,
                TriTableMem))
    {
        SDL_SetError(
                    "Couldn't copy edge table to device\n%s",
                    SDL_GetError());
        return false;
    }

    WriteDescriptorSet();

    return true;
}

void MarchingCubes::WriteDescriptorSet()
{
    VkDescriptorBufferInfo inputInfo =
    {
        InputBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo outputInfo =
    {
        VertexBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo edgeTableInfo =
    {
        EdgeTableBuf,
        0,
        VK_WHOLE_SIZE
    };
    VkDescriptorBufferInfo triTableInfo =
    {
        TriTableBuf,
        0,
        VK_WHOLE_SIZE
    };

    std::array<VkWriteDescriptorSet, 4> descriptorWrites =
    {
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSet,
            0,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &inputInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSet,
            1,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &outputInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSet,
            2,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &edgeTableInfo,
            VK_NULL_HANDLE
        },
        VkWriteDescriptorSet
        {
            VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            nullptr,
            DescriptorSet,
            3,
            0,
            1,
            VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            VK_NULL_HANDLE,
            &triTableInfo,
            VK_NULL_HANDLE
        }
    };

    VkManager->UpdateDescriptorSets(
                descriptorWrites.data(),
                (uint32_t)descriptorWrites.size());
}
