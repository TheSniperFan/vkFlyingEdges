#pragma once

#include <SDL.h>
#include <string>

#include "coreTypes.h"


namespace Core
{
    /**
     * @brief The SDLInput struct is used to cache the current input state
     * of the application. This should be updated early on during each frame.
     */
    struct SDLInput
    {
        float MouseX = 0.0f;
        float MouseY = 0.0f;
        float MouseW = 0.0f;

        SDL_bool Escape = SDL_FALSE;
        SDL_bool Shift = SDL_FALSE;
        SDL_bool Ctrl = SDL_FALSE;
        SDL_bool MouseL = SDL_FALSE;
        SDL_bool MouseR = SDL_FALSE;

        SDL_bool One = SDL_FALSE;
        SDL_bool Two = SDL_FALSE;
        SDL_bool Three = SDL_FALSE;
        SDL_bool Four = SDL_FALSE;

        SDL_bool W = SDL_FALSE;
        SDL_bool R = SDL_FALSE;
    };

    /**
     * @brief The SDLManager class sets up and configures a window for our
     * vulkan application using SDL2.
     */
    class SDLManager
    {
    public:
        SDLManager(
                bool &OutSuccess,
                int &OutErrorCode,
                std::string &OutErrorString);
        ~SDLManager();

        /**
         * @brief SDLState Getter for the SDL state.
         * @return Structure containing all relevant state information for SDL.
         */
        SDLStateInfo &SDLState()
        {
            return m_SDLState;
        }

        /**
         * @return Current input information.
         */
        SDLInput CurrentInput()
        {
            return m_CurrentInput;
        }

        /**
         * @brief ShouldQuit returns SDL_TRUE, if the SDL_QUIT signal should
         * be handled.
         * @return Whether the application should quit.
         */
        SDL_bool ShouldQuit()
        {
            return m_ShouldQuit;
        }

        /**
         * @brief Update updates the internal state of the SDL manager. It
         * handles events and updates the current input information.
         */
        void Update();

    private:
        /**
         * @brief m_SDLState Stores the data required for SDL to work.
         */
        SDLStateInfo m_SDLState;
        SDLInput m_CurrentInput;
        SDL_bool m_ShouldQuit;

        /**
         * @brief ChooseDisplay Presents a small window on multi monitor
         * configurations. The window can be dragged onto any display and
         * pressing any key will choose the display it is on for rendering.
         * @param displayIndex The display to use, if the funtion returns true.
         * @return Success.
         */
        SDL_bool ChooseDisplay(int &displayIndex);
        /**
         * @brief CreateWindow Creates a fullscreen window in the native
         * resolution on a specific display.
         * @param display Which display to create the window on.
         * @return Success.
         */
        SDL_bool CreateWindow(int display);
    };
}
