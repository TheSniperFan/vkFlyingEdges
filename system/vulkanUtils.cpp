#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE

#include "vulkanUtils.h"
#include "globalSettings.h"

#include <SDL.h>
#include <SDL_vulkan.h>
#include <vector>


Core::VulkanManager::VulkanManager(
        const SDLStateInfo &SDLState,
        bool &OutSuccess,
        int &OutErrorCode,
        std::string &OutErrorString)
{
    OutSuccess = true;
    OutErrorCode = 0;
    OutErrorString = "SUCCESS";

    SDL_memset(&m_VulkanContext, 0, sizeof(m_VulkanContext));

    if(!CreateInstance(SDLState))
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateInstance() failed with: ") + SDL_GetError();
        return;
    }
    if(!CreateSurface(SDLState))
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateSurface() failed with: ") + SDL_GetError();
        return;
    }
    if(!CreateDevice())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateDevice() failed with: ") + SDL_GetError();
        return;
    }
    CreateQueues();
    if(!CreateSwapchain(SDLState))
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateSwapchain() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateSwapchainImageViews())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateImageViews() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateRenderPass())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateRenderPass() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateGraphicsDescriptorSetLayout())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateDescriptorSetLayout() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateGraphicsDescriptorPool())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateDescriptorPool() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateGraphicsPipeline())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateGraphicsPipeline() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateCommandPool())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateCommandPool() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateDepthResources())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateDepthResources() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateFramebuffers())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateFramebuffers() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateGraphicsDescriptorSet())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateDescriptorSet() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateUniformBuffer())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateUniformBuffer() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateCommandBuffers())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateCommandBuffers() failed with: ")
                + SDL_GetError();
        return;
    }
    if(!CreateSemaphores())
    {
        OutSuccess = false;
        OutErrorCode = 2;
        OutErrorString =
                std::string("CreateSemaphores() failed with: ")
                + SDL_GetError();
        return;
    }
}

Core::VulkanManager::~VulkanManager()
{
    if(m_VulkanContext.UBOMem)
    {
        FreeDeviceMemory(
                    m_VulkanContext.UBOMem);
    }
    if(m_VulkanContext.UBOBuf)
    {
        DestroyBuffer(
                    m_VulkanContext.UBOBuf);
    }
    if(m_VulkanContext.ImageAvailableSemaphore)
    {
        vkDestroySemaphore(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.ImageAvailableSemaphore,
                    nullptr);
    }
    if(m_VulkanContext.RenderFinishedSemaphore)
    {
        vkDestroySemaphore(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.RenderFinishedSemaphore,
                    nullptr);
    }

    for(int i = 0; i < 2; ++i)
    {
        if(m_VulkanContext.CommandBuffers[i].size() > 0
                && m_VulkanContext.CommandPool)
        {
            vkFreeCommandBuffers(
                        m_VulkanContext.LogicalDevice,
                        m_VulkanContext.CommandPool,
                        (uint32_t)m_VulkanContext.CommandBuffers[i].size(),
                        m_VulkanContext.CommandBuffers[i].data());
            m_VulkanContext.CommandBuffers[i].clear();
        }
    }
    if(m_VulkanContext.DescriptorPool)
    {
        DestroyDescriptorPool(
                    m_VulkanContext.DescriptorPool);
    }
    if(m_VulkanContext.DepthImageView)
    {
        vkDestroyImageView(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.DepthImageView,
                    nullptr);
    }
    if(m_VulkanContext.DepthImage)
    {
        vkDestroyImage(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.DepthImage,
                    nullptr);
    }
    if(m_VulkanContext.DepthImageMemory)
    {
        FreeDeviceMemory(m_VulkanContext.DepthImageMemory);
    }
    if(m_VulkanContext.CommandPool)
    {
        vkDestroyCommandPool(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.CommandPool,
                    nullptr);
    }
    if(m_VulkanContext.SwapchainFramebuffers.size() > 0)
    {
        for(auto buf : m_VulkanContext.SwapchainFramebuffers)
        {
            if(buf)
            {
                vkDestroyFramebuffer(
                            m_VulkanContext.LogicalDevice,
                            buf,
                            nullptr);
            }
        }
        m_VulkanContext.SwapchainFramebuffers.clear();
    }
    if(m_VulkanContext.DescriptorSetLayout)
    {
        DestroyDescriptorSetLayout(
                    m_VulkanContext.DescriptorSetLayout);
    }
    if(m_VulkanContext.GraphicsPipelines[0])
    {
        DestroyPipeline(m_VulkanContext.GraphicsPipelines[0]);
    }
    if(m_VulkanContext.GraphicsPipelines[1])
    {
        DestroyPipeline(m_VulkanContext.GraphicsPipelines[1]);
    }
    if(m_VulkanContext.PipelineLayout)
    {
        DestroyPipelineLayout(m_VulkanContext.PipelineLayout);
    }
    if(m_VulkanContext.Renderpass)
    {
        vkDestroyRenderPass(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.Renderpass,
                    nullptr);
    }
    if(m_VulkanContext.SwapchainImageViews.size() > 0)
    {
        for(auto img : m_VulkanContext.SwapchainImageViews)
        {
            if(img)
            {
                vkDestroyImageView(
                            m_VulkanContext.LogicalDevice,
                            img,
                            nullptr);
            }
        }
        m_VulkanContext.SwapchainImageViews.clear();
    }
    if(m_VulkanContext.Swapchain)
    {
        vkDestroySwapchainKHR(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.Swapchain,
                    nullptr);
    }
    if(m_VulkanContext.Surface)
    {
        vkDestroySurfaceKHR(
                    m_VulkanContext.Instance,
                    m_VulkanContext.Surface,
                    nullptr);
    }
    if(m_VulkanContext.LogicalDevice)
    {
        vkDestroyDevice(m_VulkanContext.LogicalDevice, nullptr);
    }
    if(m_VulkanContext.Instance)
    {
        vkDestroyInstance(m_VulkanContext.Instance, nullptr);
    }
}

bool Core::VulkanManager::CreateInstance(const SDLStateInfo &SDLState)
{
    uint32_t count = 0;
    SDL_bool success = SDL_Vulkan_GetInstanceExtensions(
                SDLState.Window,
                &count,
                nullptr);
    if(!success)
    {
        SDL_SetError("Couldn't get instance extension count.");
        return false;
    }
    else if(count < 2)
    {
        SDL_SetError("Not all required instance extensions available.");
        return false;
    }

    std::vector<const char*> extensions(count);
    success = SDL_Vulkan_GetInstanceExtensions(
                SDLState.Window,
                &count,
                extensions.data());
    if(!success)
    {
        SDL_SetError("Couldn't get instance extensions.");
        return false;
    }


    VkApplicationInfo app = {};
    app.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app.pNext = nullptr;
    app.apiVersion = VK_API_VERSION_1_0;
    app.applicationVersion = APPLICATION_VERSION;
    app.engineVersion = ENGINE_VERSION;
    app.pApplicationName = APPLICATION_NAME;
    app.pEngineName = ENGINE_NAME;

    VkInstanceCreateInfo inst = {};
    inst.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    inst.pNext = nullptr;
    inst.flags = 0;
    inst.pApplicationInfo = &app;
    inst.enabledExtensionCount = count;
    inst.ppEnabledExtensionNames = extensions.data();
    inst.enabledLayerCount = 0;
    inst.ppEnabledLayerNames = nullptr;

    VkResult res = vkCreateInstance(
                &inst,
                nullptr,
                &m_VulkanContext.Instance);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create vulkan instance: %i", res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateSurface(const SDLStateInfo &SDLState)
{
    SDL_bool success = SDL_Vulkan_CreateSurface(
                SDLState.Window,
                m_VulkanContext.Instance,
                &m_VulkanContext.Surface);
    if(!success)
    {
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateDevice()
{
    VkResult res;

    // Get physical device
    uint32_t deviceCount;
    {
        std::vector<VkPhysicalDevice> devices;
        res = vkEnumeratePhysicalDevices(
                    m_VulkanContext.Instance,
                    &deviceCount,
                    nullptr);
        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't enumerate physical devices: %i", res);
            return false;
        }
        else if(deviceCount < 1)
        {
            SDL_SetError("Couldn't find at least one physical device.");
            return false;
        }

        devices.resize(deviceCount);
        res = vkEnumeratePhysicalDevices(
                    m_VulkanContext.Instance,
                    &deviceCount,
                    devices.data());
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't enumerate physical devices (%i)",
                        res);
            return false;
        }

        // NOTE: Usually you'd want to choose which device to use,
        // but this is good enough for my purposes.
        m_VulkanContext.PhysicalDevice = devices[0];

        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(devices[0], &props);

        m_VulkanContext.DeviceLimits = props.limits;

        if(props.limits.maxPushConstantsSize <
                VERT_PUSH_CONSTANT_SIZE)
        {
            SDL_SetError(
                        "Device doesn't support large enough push constants!\n"
                        "Needed: %lu\nSupported: %u",
                        VERT_PUSH_CONSTANT_SIZE,
                        props.limits.maxPushConstantsSize);
            return false;
        }
    }

    std::vector<const char*> requiredExtensions {
        REQUIRED_DEVICE_EXTENSIONS
    };
    // Enumerate device extensions
    {
        uint32_t extensionCount;
        std::vector<VkExtensionProperties> props;
        res = vkEnumerateDeviceExtensionProperties(
                    m_VulkanContext.PhysicalDevice,
                    nullptr,
                    &extensionCount,
                    nullptr);
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't get device extension property count: %i",
                        res);
            return false;
        }

        props.resize(extensionCount);
        res = vkEnumerateDeviceExtensionProperties(
                    m_VulkanContext.PhysicalDevice,
                    nullptr,
                    &extensionCount,
                    props.data());
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't enumerate device extension properties: %i",
                        res);
            return false;
        }

        for(auto reqExt : requiredExtensions)
        {
            bool found = false;
            for(auto ext : props)
            {
                if(SDL_strcmp(ext.extensionName, reqExt))
                {
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                SDL_SetError(
                            "Device extension \"%s\" not supported.",
                            reqExt);
                return false;
            }
        }
    }

    // Find device queue families for graphics and compute
    {
        uint32_t queueFamiliesCount;
        std::vector<VkQueueFamilyProperties> props;
        vkGetPhysicalDeviceQueueFamilyProperties(
                    m_VulkanContext.PhysicalDevice,
                    &queueFamiliesCount,
                    nullptr);

        if(queueFamiliesCount < 1)
        {
            SDL_SetError("Couldn't get queue families properties count.");
            return false;
        }

        props.resize(queueFamiliesCount);
        vkGetPhysicalDeviceQueueFamilyProperties(
                    m_VulkanContext.PhysicalDevice,
                    &queueFamiliesCount,
                    props.data());

        bool foundGraphics = false, foundCompute = false;
        for (
             uint32_t i = 0;
             i < queueFamiliesCount
             && !(foundGraphics && foundCompute);
             ++i)
        {
            VkQueueFamilyProperties &q = props[i];
            if(q.queueCount < 1) { continue; }
            if(!foundGraphics)
            {
                if(q.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                {
                    VkBool32 supports_presentation = VK_FALSE;
                    res = vkGetPhysicalDeviceSurfaceSupportKHR(
                                m_VulkanContext.PhysicalDevice,
                                i,
                                m_VulkanContext.Surface,
                                &supports_presentation);
                    if(res != VK_SUCCESS)
                    {
                        SDL_SetError("Couldn't get physical device surface "
                                     "support information: %i",
                                     res);
                        return false;
                    }

                    if(supports_presentation)
                    {
                        m_VulkanContext.GraphicsQueueFamilyIndex = i;
                        foundGraphics = true;
                    }
                }
            }
            if(!foundCompute)
            {
                if(q.queueFlags & VK_QUEUE_COMPUTE_BIT)
                {
                    m_VulkanContext.ComputeQueueFamilyIndex = i;
                    foundCompute = true;
                }
            }
        }
        if(foundGraphics == false)
        {
            SDL_SetError("Couldn't find graphics queue that "
                         "supports presentation.");
            return false;
        }
        if(foundCompute == false)
        {
            SDL_SetError("Couldn't find compute queue.");
            return false;
        }

        m_VulkanContext.QueueCreateInfo.push_back(
                    QueueInfo {
                        m_VulkanContext.GraphicsQueueFamilyIndex,
                        { GRAPHICS_QUEUE_PRIORITY }
                    });
        if(m_VulkanContext.GraphicsQueueFamilyIndex ==
                m_VulkanContext.ComputeQueueFamilyIndex)
        {
            m_VulkanContext.QueueCreateInfo[0].Priorities.push_back(
                        COMPUTE_QUEUE_PRIORITY);
        }
        else
        {
            m_VulkanContext.QueueCreateInfo.push_back(
                        QueueInfo {
                            m_VulkanContext.ComputeQueueFamilyIndex,
                            { COMPUTE_QUEUE_PRIORITY }
                        });
        }
    }

    // Create device queue info
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfo;
    {
        for (size_t i = 0; i < m_VulkanContext.QueueCreateInfo.size(); ++i)
        {
            const QueueInfo &qi = m_VulkanContext.QueueCreateInfo[i];
            queueCreateInfo.push_back(
                        VkDeviceQueueCreateInfo{
                            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                            nullptr,
                            0,
                            qi.FamilyIndex,
                            (uint32_t)qi.Priorities.size(),
                            qi.Priorities.data()
                        });
        }
    }

    // Create device
    VkDeviceCreateInfo devInfo =
    {
        VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        nullptr,
        0,
        (uint32_t)queueCreateInfo.size(),
        queueCreateInfo.data(),
        0,
        nullptr,
        (uint32_t)requiredExtensions.size(),
        requiredExtensions.data(),
        nullptr
    };

    res = vkCreateDevice(
                m_VulkanContext.PhysicalDevice,
                &devInfo,
                nullptr,
                &m_VulkanContext.LogicalDevice);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create logical vulkan device: %i", res);
        return false;
    }

    return true;
}

void Core::VulkanManager::CreateQueues()
{
    vkGetDeviceQueue(
                m_VulkanContext.LogicalDevice,
                m_VulkanContext.GraphicsQueueFamilyIndex,
                0,
                &m_VulkanContext.GraphicsQueue);

    uint32_t cIndex =
            (m_VulkanContext.GraphicsQueueFamilyIndex !=
             m_VulkanContext.ComputeQueueFamilyIndex) ? 0 : 1;
    vkGetDeviceQueue(
                m_VulkanContext.LogicalDevice,
                m_VulkanContext.ComputeQueueFamilyIndex,
                cIndex,
                &m_VulkanContext.ComputeQueue);
}

bool Core::VulkanManager::CreateSwapchain(const SDLStateInfo &SDLState)
{
    // Choose present mode
    VkPresentModeKHR desiredPresentMode = DESIRED_PRESENT_MODE;
    uint32_t presentModesCount;
    VkResult res = vkGetPhysicalDeviceSurfacePresentModesKHR(
                m_VulkanContext.PhysicalDevice,
                m_VulkanContext.Surface,
                &presentModesCount,
                nullptr);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't get present modes count (%i)", res);
        return false;
    }
    else if(presentModesCount < 1)
    {
        SDL_SetError("No present modes found.");
        return false;
    }

    std::vector<VkPresentModeKHR> presentModes(presentModesCount);
    res = vkGetPhysicalDeviceSurfacePresentModesKHR(
                m_VulkanContext.PhysicalDevice,
                m_VulkanContext.Surface,
                &presentModesCount,
                presentModes.data());
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't get present modes (%i)", res);
        return false;
    }

    if(!SupportsPresentMode(presentModes, desiredPresentMode))
    {
        SDL_Log("Desired present mode not supported! Falling back ...");
        desiredPresentMode = DESIRED_FALLBACK_PRESENT_MODE;

        if(!SupportsPresentMode(presentModes, desiredPresentMode))
        {
            SDL_Log("Desired fallback present mode not supported! Falling back"
                    "to VK_PRESENT_MODE_FIFO_KHR ...");
            desiredPresentMode = VK_PRESENT_MODE_FIFO_KHR;
        }
    }


    // Get surface capabilities
    VkSurfaceCapabilitiesKHR surfaceCaps;
    res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
                m_VulkanContext.PhysicalDevice,
                m_VulkanContext.Surface,
                &surfaceCaps);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't get surface capabilities (%i)", res);
        return false;
    }


    // Select number of swapchain images
    uint32_t numImages = surfaceCaps.minImageCount + 1;
    if(surfaceCaps.maxImageCount > 0
            && numImages > surfaceCaps.maxImageCount)
    {
        numImages = surfaceCaps.maxImageCount;
    }


    // Select the swapchain image usage
    VkImageUsageFlags desiredImageUsage = DESIRED_IMAGE_USAGE_FLAGS;
    VkImageUsageFlags imageUsage =
            desiredImageUsage & surfaceCaps.supportedUsageFlags;
    if(desiredImageUsage != imageUsage)
    {
        SDL_SetError("Required image usage flags not supported.");
        return false;
    }


    // Select surface format and color space
    VkColorSpaceKHR imageColorSpace;
    {
        uint32_t formatCount;
        res = vkGetPhysicalDeviceSurfaceFormatsKHR(
                    m_VulkanContext.PhysicalDevice,
                    m_VulkanContext.Surface,
                    &formatCount,
                    nullptr);
        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't get surface format count (%i)", res);
            return false;
        }
        else if(formatCount < 1)
        {
            SDL_SetError("No surface formats supported");
            return false;
        }

        std::vector<VkSurfaceFormatKHR> supportedFormats(formatCount);
        res = vkGetPhysicalDeviceSurfaceFormatsKHR(
                    m_VulkanContext.PhysicalDevice,
                    m_VulkanContext.Surface,
                    &formatCount,
                    supportedFormats.data());
        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't get surface formats");
            return false;
        }

        SetImageFormatAndColorSpace(
                    supportedFormats,
                    m_VulkanContext.ImageFormat,
                    imageColorSpace);
    }


    // Store the size of the swapchain images
    {
        int w, h;
        SDL_Vulkan_GetDrawableSize(
                    SDLState.Window,
                    &w,
                    &h);
        m_VulkanContext.ImageExtent.height = (uint32_t)h;
        m_VulkanContext.ImageExtent.width = (uint32_t)w;
    }

    // Create the swapchain
    VkSwapchainCreateInfoKHR swapchainCreateInfo =
    {
        VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        nullptr,
        0,
        m_VulkanContext.Surface,
        numImages,
        m_VulkanContext.ImageFormat,
        imageColorSpace,
        m_VulkanContext.ImageExtent,
        1,
        imageUsage,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        surfaceCaps.currentTransform,
        VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        desiredPresentMode,
        VK_TRUE,
        VK_NULL_HANDLE
    };
    res = vkCreateSwapchainKHR(
                m_VulkanContext.LogicalDevice,
                &swapchainCreateInfo,
                nullptr,
                &m_VulkanContext.Swapchain);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create swapchain (%i)", res);
        return false;
    }


    // Get handles to the images
    {
        uint32_t imageCount;
        res = vkGetSwapchainImagesKHR(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.Swapchain,
                    &imageCount,
                    nullptr);
        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't get swapchain image count (%i)", res);
            return false;
        }
        m_VulkanContext.SwapchainImages.resize(imageCount);
        res = vkGetSwapchainImagesKHR(
                    m_VulkanContext.LogicalDevice,
                    m_VulkanContext.Swapchain,
                    &imageCount,
                    m_VulkanContext.SwapchainImages.data());
        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't get swapchain images (%i)", res);
            return false;
        }
    }

    return true;
}

bool Core::VulkanManager::SupportsPresentMode(
        std::vector<VkPresentModeKHR> &InSupportedModes,
        VkPresentModeKHR Mode)
{
    bool ret = false;
    for(auto m : InSupportedModes)
    {
        if(m == Mode)
        {
            ret = true;
            break;
        }
    }
    return ret;
}

void Core::VulkanManager::SetImageFormatAndColorSpace(
        std::vector<VkSurfaceFormatKHR> &InSupportedFormats,
        VkFormat &OutFormat,
        VkColorSpaceKHR &OutColorSpace)
{
    // Supports everything
    if(InSupportedFormats.size() == 1
            && InSupportedFormats[0].format == VK_FORMAT_UNDEFINED)
    {
        OutFormat = DESIRED_IMAGE_FORMAT;
        OutColorSpace = DESIRED_COLOR_SPACE;
        return;
    }

    // Supports desired format and color space
    for(auto f : InSupportedFormats)
    {
        if(f.format == DESIRED_IMAGE_FORMAT
                && f.colorSpace == DESIRED_COLOR_SPACE)
        {
            OutFormat = DESIRED_IMAGE_FORMAT;
            OutColorSpace = DESIRED_COLOR_SPACE;
            return;
        }
    }

    // Supports format, but not color space
    for(auto f : InSupportedFormats)
    {
        if(f.format == DESIRED_IMAGE_FORMAT)
        {
            OutFormat = DESIRED_IMAGE_FORMAT;
            OutColorSpace = f.colorSpace;
            return;
        }
    }

    // Supports neither
    OutFormat = InSupportedFormats[0].format;
    OutColorSpace = InSupportedFormats[0].colorSpace;
}

bool Core::VulkanManager::CreateSwapchainImageViews()
{
    int numImages = (int)m_VulkanContext.SwapchainImages.size();
    m_VulkanContext.SwapchainImageViews.resize(numImages);

    for (int i = 0; i < numImages; ++i)
    {
        VkImage &img = m_VulkanContext.SwapchainImages[i];
        if(!CreateImageView(
                    img,
                    m_VulkanContext.ImageFormat,
                    VK_IMAGE_ASPECT_COLOR_BIT,
                    m_VulkanContext.SwapchainImageViews[i]))
        {
            SDL_SetError(
                        "CreateImage(%i) failed with:\n%s",
                        i, SDL_GetError());
            return false;
        }
    }

    return true;
}

bool Core::VulkanManager::CreateRenderPass()
{
    VkAttachmentDescription colorAttachment =
    {
        0,
        m_VulkanContext.ImageFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };

    VkAttachmentReference colorAttachmentRef =
    {
        0,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkFormat depthFormat;
    FindSupportedDepthFormat(depthFormat);

    VkAttachmentDescription depthAttachment =
    {
        0,
        depthFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentReference depthAttachmentRef =
    {
        1,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription subpass =
    {
        0,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        0,
        nullptr,
        1,
        &colorAttachmentRef,
        nullptr,
        &depthAttachmentRef,
        0,
        nullptr

    };

    VkSubpassDependency dependency =
    {
        VK_SUBPASS_EXTERNAL,
        0,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        0,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
        | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        0
    };

    std::array<VkAttachmentDescription, 2> attachments =
    {
        colorAttachment,
        depthAttachment
    };

    VkRenderPassCreateInfo renderPassInfo =
    {
        VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        nullptr,
        0,
        (uint32_t)attachments.size(),
        attachments.data(),
        1,
        &subpass,
        1,
        &dependency
    };

    VkResult res = vkCreateRenderPass(
                m_VulkanContext.LogicalDevice,
                &renderPassInfo,
                nullptr,
                &m_VulkanContext.Renderpass);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create render pass (%i)", res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateGraphicsDescriptorSetLayout()
{
    VkDescriptorSetLayoutBinding binding =
    {
        0,
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        1,
        VK_SHADER_STAGE_VERTEX_BIT |
        VK_SHADER_STAGE_FRAGMENT_BIT,
        nullptr
    };

    VkDescriptorSetLayoutCreateInfo layoutInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        nullptr,
        0,
        1,
        &binding
    };

    VkResult res = vkCreateDescriptorSetLayout(
                m_VulkanContext.LogicalDevice,
                &layoutInfo,
                nullptr,
                &m_VulkanContext.DescriptorSetLayout);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create descriptor set layout (%i)",
                     res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateGraphicsPipeline()
{
    VkShaderModule vertexMod, fragmentMod;
    {
        std::vector<char> shader;
        if(!LoadShaderFromDisk(
                    shader,
                    VERTEX_SHADER_PATH))
        {
            SDL_SetError(
                        "LoadShader(vertex) failed with:\n%s",
                        SDL_GetError());
            return false;
        }
        if(!CreateShaderModule(
                    shader,
                    vertexMod))
        {
            SDL_SetError(
                        "CreateShaderModule(vertex) failed with:\n%s",
                        SDL_GetError());
            return false;
        }

        shader.clear();
        if(!LoadShaderFromDisk(
                    shader,
                    FRAGMENT_SHADER_PATH))
        {
            SDL_SetError(
                        "LoadShader(fragment) failed with:\n%s",
                        SDL_GetError());
            return false;
        }
        if(!CreateShaderModule(
                    shader,
                    fragmentMod))
        {
            SDL_SetError(
                        "CreateShaderModule(fragment) failed with:\n%s",
                        SDL_GetError());
            return false;
        }
    }

    VkPipelineShaderStageCreateInfo vertCreateStageInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,
        0,
        VK_SHADER_STAGE_VERTEX_BIT,
        vertexMod,
        "main",
        nullptr
    };

    VkPipelineShaderStageCreateInfo fragCreateStageInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        nullptr,
        0,
        VK_SHADER_STAGE_FRAGMENT_BIT,
        fragmentMod,
        "main",
        nullptr
    };

    VkPipelineShaderStageCreateInfo shaderStages[] =
    {
        vertCreateStageInfo,
        fragCreateStageInfo
    };

    auto bindingDescription = Model::Vertex::BindingDescription();
    auto attributeDescriptions = Model::Vertex::AttributeDescriptions();

    VkPipelineVertexInputStateCreateInfo vertexInputInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        nullptr,
        0,
        1,
        &bindingDescription,
        (uint32_t)attributeDescriptions.size(),
        attributeDescriptions.data()
    };

    VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        VK_FALSE
    };

    VkViewport viewport =
    {
        0.0f,
        0.0f,
        (float)m_VulkanContext.ImageExtent.width,
        (float)m_VulkanContext.ImageExtent.height,
        0.0f,
        1.0f
    };

    VkRect2D scissor =
    {
        VkOffset2D
        {
            0,
            0
        },
        m_VulkanContext.ImageExtent
    };

    VkPipelineViewportStateCreateInfo viewportStateInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        nullptr,
        0,
        1,
        &viewport,
        1,
        &scissor
    };

    VkPipelineRasterizationStateCreateInfo rasterizerInfo_0 =
    {
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_FALSE,
        VK_FALSE,
        VK_POLYGON_MODE_FILL,
        VK_CULL_MODE_NONE,
        VK_FRONT_FACE_COUNTER_CLOCKWISE,
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f
    };

    VkPipelineRasterizationStateCreateInfo rasterizerInfo_1 =
    {
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_FALSE,
        VK_FALSE,
        VK_POLYGON_MODE_LINE,
        VK_CULL_MODE_FRONT_BIT,
        VK_FRONT_FACE_COUNTER_CLOCKWISE,
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f
    };

    VkPipelineMultisampleStateCreateInfo multisamplingInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_SAMPLE_COUNT_1_BIT,
        VK_FALSE,
        1.0f,
        nullptr,
        VK_FALSE,
        VK_FALSE
    };

    VkPipelineDepthStencilStateCreateInfo depthStencilInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_TRUE,
        VK_TRUE,
        VK_COMPARE_OP_LESS,
        VK_FALSE,
        VK_FALSE,
        VkStencilOpState {},
        VkStencilOpState {},
        0.0f,
        1.0f
    };

    VkPipelineColorBlendAttachmentState colorBlendAttachment =
    {
        VK_FALSE,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ZERO,
        VK_BLEND_OP_ADD,
        VK_BLEND_FACTOR_ONE,
        VK_BLEND_FACTOR_ZERO,
        VK_BLEND_OP_ADD,
        VK_COLOR_COMPONENT_R_BIT
        | VK_COLOR_COMPONENT_G_BIT
        | VK_COLOR_COMPONENT_B_BIT
        | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo colorBlendingInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        nullptr,
        0,
        VK_FALSE,
        VK_LOGIC_OP_COPY,
        1,
        &colorBlendAttachment,
        {
            0.0f,
            0.0f,
            0.0f,
            0.0f
        }
    };

    VkPipelineLayoutCreateInfo pipelineLayoutInfo =
    {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        nullptr,
        0,
        1,
        &m_VulkanContext.DescriptorSetLayout,
        0,
        VK_NULL_HANDLE
    };

    VkResult res = vkCreatePipelineLayout(
                m_VulkanContext.LogicalDevice,
                &pipelineLayoutInfo,
                nullptr,
                &m_VulkanContext.PipelineLayout);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create pipeline layout (%i)", res);
        return false;
    }

    std::array<VkGraphicsPipelineCreateInfo, 2> pipelineInfos =
    {
        VkGraphicsPipelineCreateInfo
        {
            VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            nullptr,
            0,
            2,
            shaderStages,
            &vertexInputInfo,
            &inputAssemblyInfo,
            nullptr,
            &viewportStateInfo,
            &rasterizerInfo_0,
            &multisamplingInfo,
            &depthStencilInfo,
            &colorBlendingInfo,
            nullptr,
            m_VulkanContext.PipelineLayout,
            m_VulkanContext.Renderpass,
            0,
            VK_NULL_HANDLE,
            -1
        },
        VkGraphicsPipelineCreateInfo
        {
            VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            nullptr,
            0,
            2,
            shaderStages,
            &vertexInputInfo,
            &inputAssemblyInfo,
            nullptr,
            &viewportStateInfo,
            &rasterizerInfo_1,
            &multisamplingInfo,
            &depthStencilInfo,
            &colorBlendingInfo,
            nullptr,
            m_VulkanContext.PipelineLayout,
            m_VulkanContext.Renderpass,
            0,
            VK_NULL_HANDLE,
            -1
        }
    };

    res = vkCreateGraphicsPipelines(
                m_VulkanContext.LogicalDevice,
                VK_NULL_HANDLE,
                2,
                pipelineInfos.data(),
                nullptr,
                m_VulkanContext.GraphicsPipelines);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create graphcis pipeline (%i)", res);
        return false;
    }

    if(vertexMod)
    {
        vkDestroyShaderModule(
                    m_VulkanContext.LogicalDevice,
                    vertexMod,
                    nullptr);
    }
    if(fragmentMod)
    {
        vkDestroyShaderModule(
                    m_VulkanContext.LogicalDevice,
                    fragmentMod,
                    nullptr);
    }

    return true;
}

bool Core::VulkanManager::LoadShaderFromDisk(
        std::vector<char> &OutShader,
        const char *Path)
{
    SDL_RWops *io = nullptr;
    io = SDL_RWFromFile(Path, "rb");
    if(!io)
    {
        return false;
    }
    size_t size = (size_t)SDL_RWsize(io);
    if(size < 1)
    {
        SDL_SetError("Shader appears to be empty.");
        SDL_RWclose(io);
        return false;
    }

    OutShader.resize(size);
    SDL_RWread(io, OutShader.data(), size, 1);
    SDL_RWclose(io);
    return true;
}

bool Core::VulkanManager::CreateShaderModule(
        const std::vector<char> &InShader,
        VkShaderModule &OutModule)
{
    VkShaderModuleCreateInfo info =
    {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        nullptr,
        0,
        InShader.size(),
        reinterpret_cast<const uint32_t*>(InShader.data())
    };

    VkResult res = vkCreateShaderModule(
                m_VulkanContext.LogicalDevice,
                &info,
                nullptr,
                &OutModule);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create shader module (%i)", res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateFramebuffers()
{
    size_t size = m_VulkanContext.SwapchainImageViews.size();
    m_VulkanContext.SwapchainFramebuffers.resize(size);
    VkResult res;
    for (size_t i = 0; i < size; ++i)
    {
        std::array<VkImageView,2> attachments =
        {
            m_VulkanContext.SwapchainImageViews[i],
            m_VulkanContext.DepthImageView
        };

        VkFramebufferCreateInfo framebufferInfo =
        {
            VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            nullptr,
            0,
            m_VulkanContext.Renderpass,
            (uint32_t)attachments.size(),
            attachments.data(),
            m_VulkanContext.ImageExtent.width,
            m_VulkanContext.ImageExtent.height,
            1
        };


        res = vkCreateFramebuffer(
                    m_VulkanContext.LogicalDevice,
                    &framebufferInfo,
                    nullptr,
                    &m_VulkanContext.SwapchainFramebuffers[i]);
        if(res != VK_SUCCESS)
        {
            SDL_SetError(
                        "Couldn't create framebuffer %i (%i)",
                        (int)i,
                        res);
            return false;
        }
    }

    return true;
}

bool Core::VulkanManager::CreateCommandPool()
{
    VkCommandPoolCreateInfo poolInfo =
    {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        nullptr,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        m_VulkanContext.GraphicsQueueFamilyIndex
    };

    VkResult res = vkCreateCommandPool(
                m_VulkanContext.LogicalDevice,
                &poolInfo,
                nullptr,
                &m_VulkanContext.CommandPool);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create a command pool (%i)", res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateDepthResources()
{
    VkFormat depthFormat;
    if (!FindSupportedDepthFormat(depthFormat))
    {
        SDL_SetError("Couldn't find supported depth format");
        return false;
    }

    if(!CreateImage(
                m_VulkanContext.ImageExtent.width,
                m_VulkanContext.ImageExtent.height,
                depthFormat,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                m_VulkanContext.DepthImage,
                m_VulkanContext.DepthImageMemory))
    {
        SDL_SetError(
                    "CreateImage(depth) failed with:\n%s",
                    SDL_GetError());
        return false;
    }
    if(!CreateImageView(
                m_VulkanContext.DepthImage,
                depthFormat,
                VK_IMAGE_ASPECT_DEPTH_BIT,
                m_VulkanContext.DepthImageView))
    {
        SDL_SetError(
                    "CreateImageView(depth) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    TransitionImageLayout(
                m_VulkanContext.DepthImage,
                depthFormat,
                VK_IMAGE_LAYOUT_UNDEFINED,
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    return true;
}

bool Core::VulkanManager::CreateGraphicsDescriptorPool()
{
    VkDescriptorPoolSize descriptorType =
    {
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        2
    };

    VkDescriptorPoolCreateInfo poolInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        nullptr,
        0,
        1,
        1,
        &descriptorType
    };

    VkResult res = vkCreateDescriptorPool(
                m_VulkanContext.LogicalDevice,
                &poolInfo,
                nullptr,
                &m_VulkanContext.DescriptorPool);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create descriptor pool (%i)",
                    res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateGraphicsDescriptorSet()
{
    VkDescriptorSetAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        nullptr,
        m_VulkanContext.DescriptorPool,
        1,
        &m_VulkanContext.DescriptorSetLayout
    };

    VkResult res = vkAllocateDescriptorSets(
                m_VulkanContext.LogicalDevice,
                &allocInfo,
                &m_VulkanContext.DescriptorSet);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't allocate descriptor sets (%i)",
                    res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateUniformBuffer()
{
    const VkDeviceSize bufferSize =
            sizeof(UniformBufferObject);
    if (!CreateBuffer(
                bufferSize,
                VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT |
                VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT |
                VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
                m_VulkanContext.UBOBuf,
                m_VulkanContext.UBOMem))
    {
        SDL_SetError(
                    "CreateBuffer(Vertex UBO) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    VkDescriptorBufferInfo uboInfo =
    {
        m_VulkanContext.UBOBuf,
        0,
        VK_WHOLE_SIZE
    };

    VkWriteDescriptorSet descriptorWrites =
    {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        nullptr,
        m_VulkanContext.DescriptorSet,
        0,
        0,
        1,
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        VK_NULL_HANDLE,
        &uboInfo,
        VK_NULL_HANDLE
    };

    vkUpdateDescriptorSets(
                m_VulkanContext.LogicalDevice,
                1,
                &descriptorWrites,
                0,
                nullptr);

    return true;
}

bool Core::VulkanManager::CreateCommandBuffers()
{
    size_t size = m_VulkanContext.SwapchainFramebuffers.size();
    for (int i = 0; i < 2; ++i)
    {
        m_VulkanContext.CommandBuffers[i].resize(size);

        VkCommandBufferAllocateInfo allocInfo =
        {
            VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            nullptr,
            m_VulkanContext.CommandPool,
            VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            (uint32_t)size
        };

        VkResult res = vkAllocateCommandBuffers(
                    m_VulkanContext.LogicalDevice,
                    &allocInfo,
                    m_VulkanContext.CommandBuffers[i].data());


        if(res != VK_SUCCESS)
        {
            SDL_SetError("Couldn't allocate command buffers (%i)", res);
            return false;
        }
    }
    return true;
}

bool Core::VulkanManager::CreateSemaphores()
{
    VkSemaphoreCreateInfo semInfo =
    {
        VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        nullptr,
        0
    };
    VkResult res = vkCreateSemaphore(
                m_VulkanContext.LogicalDevice,
                &semInfo,
                nullptr,
                &m_VulkanContext.ImageAvailableSemaphore);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create image available semaphore (%i)",
                    res);
        return false;
    }
    res = vkCreateSemaphore(
                m_VulkanContext.LogicalDevice,
                &semInfo,
                nullptr,
                &m_VulkanContext.RenderFinishedSemaphore);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create render finished semaphore (%i)",
                    res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::FindMemoryType(
        uint32_t TypeFilter,
        VkMemoryPropertyFlags Properties,
        uint32_t &OutType)
{
    VkPhysicalDeviceMemoryProperties memProps;
    vkGetPhysicalDeviceMemoryProperties(
                m_VulkanContext.PhysicalDevice,
                &memProps);

    for (uint32_t i = 0; i < memProps.memoryTypeCount; ++i)
    {
        if(TypeFilter & (1 << i)
                && (memProps.memoryTypes[i].propertyFlags & Properties)
                == Properties)
        {
            OutType = i;
            return true;
        }
    }

    return false;
}

bool Core::VulkanManager::FindSupportedFormat(
    const std::vector<VkFormat> &Canidates,
    VkImageTiling Tiling,
    VkFormatFeatureFlags Features,
    VkFormat &OutFormat)
{
    for (auto format : Canidates)
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(
            m_VulkanContext.PhysicalDevice,
            format,
            &props);

        if (Tiling == VK_IMAGE_TILING_LINEAR
                && (props.linearTilingFeatures & Features) == Features)
        {
            OutFormat = format;
            return true;
        }
        else if (Tiling == VK_IMAGE_TILING_OPTIMAL
                 && (props.optimalTilingFeatures & Features) == Features)
        {
            OutFormat = format;
            return true;
        }
    }

    return false;
}

bool Core::VulkanManager::FindSupportedDepthFormat(VkFormat &OutFormat)
{
    return FindSupportedFormat(
        {
            VK_FORMAT_D32_SFLOAT,
            VK_FORMAT_D32_SFLOAT_S8_UINT,
            VK_FORMAT_D24_UNORM_S8_UINT
        },
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT,
        OutFormat);
}

bool Core::VulkanManager::HasStencilComponent(VkFormat Format)
{
    return Format == VK_FORMAT_D32_SFLOAT_S8_UINT
        || Format == VK_FORMAT_D24_UNORM_S8_UINT;
}

bool Core::VulkanManager::CreateBuffer(
        VkDeviceSize Size,
        VkBufferUsageFlags Usage,
        VkMemoryPropertyFlags Properties,
        VkBuffer &Buffer,
        VkDeviceMemory &Memory)
{
    VkBufferCreateInfo bufferInfo =
    {
        VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        nullptr,
        0,
        Size,
        Usage,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        VK_NULL_HANDLE
    };

    VkResult res = vkCreateBuffer(
                m_VulkanContext.LogicalDevice,
                &bufferInfo,
                nullptr,
                &Buffer);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't create buffer (%i)", res);
        return false;
    }

    VkMemoryRequirements memReq;
    vkGetBufferMemoryRequirements(
                m_VulkanContext.LogicalDevice,
                Buffer,
                &memReq);

    uint32_t memTypeIndex;
    if(!FindMemoryType(
                memReq.memoryTypeBits,
                Properties,
                memTypeIndex))
    {
        SDL_SetError("Couldn't find suitable memory type for buffer");
        return false;
    }

    VkMemoryAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        nullptr,
        memReq.size,
        memTypeIndex
    };

    res = vkAllocateMemory(
                m_VulkanContext.LogicalDevice,
                &allocInfo,
                nullptr,
                &Memory);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't allocate buffer (%i)", res);
        return false;
    }

    res = vkBindBufferMemory(
                m_VulkanContext.LogicalDevice,
                Buffer,
                Memory,
                0);
    if(res != VK_SUCCESS)
    {
        SDL_SetError("Couldn't bind buffer memory (%i)", res);
        return false;
    }

    return true;
}

void Core::VulkanManager::CopyBuffer(
        VkBuffer SrcBuffer,
        VkBuffer DstBuffer,
        VkDeviceSize Size)
{
    VkCommandBuffer commandBuffer =
            BeginSingleTimeCommands();

    VkBufferCopy copyRegion =
    {
        0,
        0,
        Size
    };
    vkCmdCopyBuffer(
                commandBuffer,
                SrcBuffer,
                DstBuffer,
                1,
                &copyRegion);

    EndSingleTimeCommands(
                commandBuffer,
                m_VulkanContext.GraphicsQueue);
}

bool Core::VulkanManager::CreateImage(
        uint32_t Width,
        uint32_t Height,
        VkFormat Format,
        VkImageTiling Tiling,
        VkImageUsageFlags Usage,
        VkMemoryPropertyFlags Properties,
        VkImage &Image,
        VkDeviceMemory &ImageMemory)
{
    VkImageCreateInfo imageInfo =
    {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        nullptr,
        0,
        VK_IMAGE_TYPE_2D,
        Format,
        VkExtent3D
        {
            Width,
            Height,
            1
        },
        1,
        1,
        VK_SAMPLE_COUNT_1_BIT,
        Tiling,
        Usage,
        VK_SHARING_MODE_EXCLUSIVE,
        0,
        nullptr,
        VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkResult res = vkCreateImage(
                m_VulkanContext.LogicalDevice,
                &imageInfo,
                nullptr,
                &Image);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create image (%i)",
                    res);
        return false;
    }

    VkMemoryRequirements memReq;
    vkGetImageMemoryRequirements(
                m_VulkanContext.LogicalDevice,
                Image,
                &memReq);
    uint32_t memType;
    if(!FindMemoryType(
                memReq.memoryTypeBits,
                Properties,
                memType))
    {
        SDL_SetError("Couldn't find memory type for image");
        return false;
    }

    VkMemoryAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        nullptr,
        memReq.size,
        memType
    };

    res = vkAllocateMemory(
                m_VulkanContext.LogicalDevice,
                &allocInfo,
                nullptr,
                &ImageMemory);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Allocating memory for image failed (%i)",
                    res);
        return false;
    }

    res = vkBindImageMemory(
                m_VulkanContext.LogicalDevice,
                Image,
                ImageMemory,
                0);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't bind image memory (%i)",
                    res);
        return false;
    }

    return true;
}

bool Core::VulkanManager::CreateImageView(
        VkImage Image,
        VkFormat Format,
        VkImageAspectFlags AspectFlags,
        VkImageView &OutView)
{
    VkImageViewCreateInfo viewInfo =
    {
        VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        nullptr,
        0,
        Image,
        VK_IMAGE_VIEW_TYPE_2D,
        Format,
        VkComponentMapping
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        VkImageSubresourceRange
        {
            AspectFlags,
            0,
            1,
            0,
            1
        }
    };

    VkResult res = vkCreateImageView(
                m_VulkanContext.LogicalDevice,
                &viewInfo,
                nullptr,
                &OutView);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't create image view (%i)",
                    res);
        return false;
    }

    return true;
}

VkCommandBuffer Core::VulkanManager::BeginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocInfo =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        nullptr,
        m_VulkanContext.CommandPool,
        VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        1
    };

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(
                m_VulkanContext.LogicalDevice,
                &allocInfo,
                &commandBuffer);
    VkCommandBufferBeginInfo beginInfo =
    {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        nullptr,
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        VK_NULL_HANDLE
    };

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void Core::VulkanManager::EndSingleTimeCommands(
        VkCommandBuffer CommandBuffer,
        VkQueue Queue)
{
    vkEndCommandBuffer(CommandBuffer);

    VkSubmitInfo submitInfo =
    {
        VK_STRUCTURE_TYPE_SUBMIT_INFO,
        nullptr,
        0,
        VK_NULL_HANDLE,
        VK_NULL_HANDLE,
        1,
        &CommandBuffer,
        0,
        VK_NULL_HANDLE
    };
    vkQueueSubmit(
                Queue,
                1,
                &submitInfo,
                VK_NULL_HANDLE);
    vkQueueWaitIdle(Queue);
    vkFreeCommandBuffers(
                m_VulkanContext.LogicalDevice,
                m_VulkanContext.CommandPool,
                1,
                &CommandBuffer);
}

bool Core::VulkanManager::TransitionImageLayout(
        VkImage Image,
        VkFormat Format,
        VkImageLayout OldLayout,
        VkImageLayout NewLayout)
{
    VkCommandBuffer commandBuffer = BeginSingleTimeCommands();

    VkImageMemoryBarrier barrier =
    {
        VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        nullptr,
        0,
        0,
        OldLayout,
        NewLayout,
        VK_QUEUE_FAMILY_IGNORED,
        VK_QUEUE_FAMILY_IGNORED,
        Image,
        VkImageSubresourceRange
        {
            VK_IMAGE_ASPECT_COLOR_BIT,
            0,
            1,
            0,
            1
        }
    };

    VkPipelineStageFlags sourceStage;
    VkPipelineStageFlags destinationStage;

    if(OldLayout == VK_IMAGE_LAYOUT_UNDEFINED
            && NewLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if(OldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
            && NewLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if(OldLayout == VK_IMAGE_LAYOUT_UNDEFINED
            && NewLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask =
                VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
                | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else
    {
        SDL_SetError("Unsupported layout transition!");
        return false;
    }

    if(NewLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        if(HasStencilComponent(Format))
        {
            barrier.subresourceRange.aspectMask |=
                    VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    }

    vkCmdPipelineBarrier(
                commandBuffer,
                sourceStage,
                destinationStage,
                0,
                0, nullptr,
                0, nullptr,
                1, &barrier);


    EndSingleTimeCommands(
                commandBuffer,
                m_VulkanContext.GraphicsQueue);
    return true;
}

bool Core::VulkanManager::CopyDataToDevice(
        VkDeviceSize BufferSize,
        void *SourceData,
        VkBufferUsageFlags Usage,
        VkBuffer &DeviceBuffer,
        VkDeviceMemory &DeviceMemory)
{
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    if(!CreateBuffer(
                BufferSize,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                stagingBuffer,
                stagingBufferMemory))
    {
        SDL_SetError(
                    "CreateBuffer(staging) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    void *data;
    VkResult res = vkMapMemory(
                m_VulkanContext.LogicalDevice,
                stagingBufferMemory,
                0,
                BufferSize,
                0,
                &data);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't map staging buffer memory (%i)",
                    res);
        return false;
    }

    if(SourceData)
    {
        SDL_memcpy(
                    data,
                    SourceData,
                    (size_t)BufferSize);
    }
    else
    {
        SDL_memset(
                    data,
                    0,
                    (size_t)BufferSize);
    }
    vkUnmapMemory(
                m_VulkanContext.LogicalDevice,
                stagingBufferMemory);

    if(!CreateBuffer(
                BufferSize,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT
                | Usage,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                DeviceBuffer,
                DeviceMemory))
    {
        SDL_SetError(
                    "CreateBuffer(device buffer) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    CopyBuffer(
                stagingBuffer,
                DeviceBuffer,
                BufferSize);
    vkDestroyBuffer(
                m_VulkanContext.LogicalDevice,
                stagingBuffer,
                nullptr);
    vkFreeMemory(
                m_VulkanContext.LogicalDevice,
                stagingBufferMemory,
                nullptr);

    return true;
}

bool Core::VulkanManager::CopyDataToDeviceDirect(
        VkDeviceSize BufferSize,
        void *Src,
        VkDeviceMemory Dst)
{
    if (!Src || !Dst)
    {
        SDL_SetError(
                    "Source or destination are invalid!");
        return false;
    }

    void *data = nullptr;
    VkResult res = vkMapMemory(
                m_VulkanContext.LogicalDevice,
                Dst,
                0,
                BufferSize,
                0,
                &data);
    if (res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't map memory! (%i)",
                    res);
        return false;
    }

    SDL_memcpy(
                data,
                Src,
                (size_t)BufferSize);

    vkUnmapMemory(
                m_VulkanContext.LogicalDevice,
                Dst);

    return true;
}

bool Core::VulkanManager::CopyDataToHost(
        VkDeviceSize BufferSize,
        const VkBuffer SourceBuffer,
        void *&DstBuffer)
{
    if(!SourceBuffer)
    {
        SDL_SetError(
                    "Invalid source buffer!");
        return false;
    }

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    if(!CreateBuffer(
                BufferSize,
                VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                stagingBuffer,
                stagingBufferMemory))
    {
        SDL_SetError(
                    "CreateBuffer(staging buffer) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    CopyBuffer(
            SourceBuffer,
            stagingBuffer,
            BufferSize);

    void *data;
    VkResult res = vkMapMemory(
                m_VulkanContext.LogicalDevice,
                stagingBufferMemory,
                0,
                BufferSize,
                0,
                &data);
    if(res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't map staging buffer memory! (%i)",
                    res);
        return false;
    }


    if(DstBuffer)
    {
        SDL_LogWarn(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "CopyDataToHost(): Destination buffer not null! "
                    "Freeing ...");
        SDL_free(DstBuffer);
    }
    DstBuffer = SDL_malloc((size_t)BufferSize);
    SDL_memcpy(
                DstBuffer,
                data,
                (size_t)BufferSize);

    vkDestroyBuffer(
                m_VulkanContext.LogicalDevice,
                stagingBuffer,
                nullptr);
    vkFreeMemory(
                m_VulkanContext.LogicalDevice,
                stagingBufferMemory,
                nullptr);

    return true;
}

bool Core::VulkanManager::CopyDataToHostDirect(
        VkDeviceSize BufferSize,
        const VkDeviceMemory Src,
        void *&Dst)
{
    if (!Src)
    {
        return false;
    }

    if (Dst)
    {
        SDL_LogWarn(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "CopyDataToHostDirect(): Destination buffer not null! "
                    "Freeing ...");
        SDL_free(Dst);
    }

    void *data = nullptr;
    VkResult res = vkMapMemory(
                m_VulkanContext.LogicalDevice,
                Src,
                0,
                BufferSize,
                0,
                &data);
    if (res != VK_SUCCESS)
    {
        SDL_SetError(
                    "Couldn't map memory (%i)",
                    res);
        return false;
    }

    Dst = SDL_malloc((size_t)BufferSize);
    SDL_memcpy(
                Dst,
                data,
                (size_t)BufferSize);

    vkUnmapMemory(
                m_VulkanContext.LogicalDevice,
                Src);

    return true;
}

void Core::VulkanManager::DestroyBuffer(
        VkBuffer Buffer)
{
    if(Buffer)
    {
        vkDestroyBuffer(
                    m_VulkanContext.LogicalDevice,
                    Buffer,
                    nullptr);
    }
}

void Core::VulkanManager::FreeDeviceMemory(
        VkDeviceMemory Memory)
{
    if(Memory)
    {
        vkFreeMemory(
                    m_VulkanContext.LogicalDevice,
                    Memory,
                    nullptr);
    }
}

bool Core::VulkanManager::UpdateRenderCommandBuffers(
        const VkBuffer *VertexBuffer,
        const int NumVerts)
{
    size_t size = m_VulkanContext.SwapchainFramebuffers.size();

    std::array<VkClearValue, 2> clearValues = {};
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[1].depthStencil = { 1.0f, 0 };

    for (int p = 0; p < 2; ++p)
    {
        for(size_t i = 0; i < size; ++i)
        {
            VkCommandBufferBeginInfo beginInfo =
            {
                VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                nullptr,
                VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
                nullptr
            };

            VkResult res = vkBeginCommandBuffer(
                        m_VulkanContext.CommandBuffers[p][i],
                        &beginInfo);
            if(res != VK_SUCCESS)
            {
                SDL_SetError(
                            "Couldn't begin command buffer %i (%i)",
                            (int)i,
                            res);
                return false;
            }

            VkRenderPassBeginInfo renderpassInfo =
            {
                VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                nullptr,
                m_VulkanContext.Renderpass,
                m_VulkanContext.SwapchainFramebuffers[i],
                VkRect2D
                {
                    { 0, 0 },
                    m_VulkanContext.ImageExtent
                },
                (uint32_t)clearValues.size(),
                clearValues.data()
            };

            vkCmdBeginRenderPass(
                        m_VulkanContext.CommandBuffers[p][i],
                        &renderpassInfo,
                        VK_SUBPASS_CONTENTS_INLINE);
            vkCmdBindPipeline(
                        m_VulkanContext.CommandBuffers[p][i],
                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                        m_VulkanContext.GraphicsPipelines[p]);
            vkCmdBindDescriptorSets(
                        m_VulkanContext.CommandBuffers[p][i],
                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                        m_VulkanContext.PipelineLayout,
                        0,
                        1,
                        &m_VulkanContext.DescriptorSet,
                        0,
                        0);

            const uint64_t offset = 0;
            vkCmdBindVertexBuffers(
                        m_VulkanContext.CommandBuffers[p][i],
                        0,
                        1,
                        VertexBuffer,
                        &offset);
            vkCmdDraw(
                        m_VulkanContext.CommandBuffers[p][i],
                        NumVerts,
                        1,
                        0,
                        0);

            vkCmdEndRenderPass(m_VulkanContext.CommandBuffers[p][i]);

            res = vkEndCommandBuffer(m_VulkanContext.CommandBuffers[p][i]);
            if(res != VK_SUCCESS)
            {
                SDL_SetError(
                            "Error while ending recording command "
                            "buffer %i (%i)",
                            (int)i,
                            res);
                return false;
            }
        }
    }

    return true;
}

bool Core::VulkanManager::UpdateRenderCommandBuffers(
        const VkBuffer *VertexBuffer,
        const VkBuffer *IndexBuffer,
        const int NumTris)
{
    size_t size = m_VulkanContext.SwapchainFramebuffers.size();

    std::array<VkClearValue, 2> clearValues = {};
    clearValues[0].color = { { 0.0f, 0.0f, 0.0f, 1.0f } };
    clearValues[1].depthStencil = { 1.0f, 0 };

    for (int p = 0; p < 2; ++p)
    {
        for(size_t i = 0; i < size; ++i)
        {
            VkCommandBufferBeginInfo beginInfo =
            {
                VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                nullptr,
                VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,
                nullptr
            };

            VkResult res = vkBeginCommandBuffer(
                        m_VulkanContext.CommandBuffers[p][i],
                        &beginInfo);
            if(res != VK_SUCCESS)
            {
                SDL_SetError(
                            "Couldn't begin command buffer %i (%i)",
                            (int)i,
                            res);
                return false;
            }

            VkRenderPassBeginInfo renderpassInfo =
            {
                VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                nullptr,
                m_VulkanContext.Renderpass,
                m_VulkanContext.SwapchainFramebuffers[i],
                VkRect2D
                {
                    { 0, 0 },
                    m_VulkanContext.ImageExtent
                },
                (uint32_t)clearValues.size(),
                clearValues.data()
            };

            vkCmdBeginRenderPass(
                        m_VulkanContext.CommandBuffers[p][i],
                        &renderpassInfo,
                        VK_SUBPASS_CONTENTS_INLINE);
            vkCmdBindPipeline(
                        m_VulkanContext.CommandBuffers[p][i],
                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                        m_VulkanContext.GraphicsPipelines[p]);
            vkCmdBindDescriptorSets(
                        m_VulkanContext.CommandBuffers[p][i],
                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                        m_VulkanContext.PipelineLayout,
                        0,
                        1,
                        &m_VulkanContext.DescriptorSet,
                        0,
                        0);

            const uint64_t offset = 0;
            vkCmdBindVertexBuffers(
                        m_VulkanContext.CommandBuffers[p][i],
                        0,
                        1,
                        VertexBuffer,
                        &offset);
            vkCmdBindIndexBuffer(
                        m_VulkanContext.CommandBuffers[p][i],
                        *IndexBuffer,
                        0,
                        VK_INDEX_TYPE_UINT32);
            vkCmdDrawIndexed(
                        m_VulkanContext.CommandBuffers[p][i],
                        NumTris * 3,
                        1,
                        0,
                        0,
                        0);

            vkCmdEndRenderPass(m_VulkanContext.CommandBuffers[p][i]);

            res = vkEndCommandBuffer(m_VulkanContext.CommandBuffers[p][i]);
            if(res != VK_SUCCESS)
            {
                SDL_SetError(
                            "Error while ending recording command "
                            "buffer %i (%i)",
                            (int)i,
                            res);
                return false;
            }
        }
    }

    return true;
}

void Core::VulkanManager::DestroyDescriptorPool(
        VkDescriptorPool &Pool)
{
    vkDestroyDescriptorPool(
                m_VulkanContext.LogicalDevice,
                Pool,
                nullptr);
}

void Core::VulkanManager::DestroyPipelineLayout(
        VkPipelineLayout &Layout)
{
    vkDestroyPipelineLayout(
                m_VulkanContext.LogicalDevice,
                Layout,
                nullptr);
}

void Core::VulkanManager::DestroyDescriptorSetLayout(
        VkDescriptorSetLayout &Layout)
{
    vkDestroyDescriptorSetLayout(
                m_VulkanContext.LogicalDevice,
                Layout,
                nullptr);
}

void Core::VulkanManager::DestroyPipeline(
        VkPipeline &Pipeline)
{
    vkDestroyPipeline(
                m_VulkanContext.LogicalDevice,
                Pipeline,
                nullptr);
}

void Core::VulkanManager::DestroyShaderModule(
        VkShaderModule &Module)
{
    vkDestroyShaderModule(
                m_VulkanContext.LogicalDevice,
                Module,
                nullptr);
}

void Core::VulkanManager::UpdateDescriptorSets(
        const VkWriteDescriptorSet *Writes,
        const uint32_t NumWrites)
{
    vkUpdateDescriptorSets(
                m_VulkanContext.LogicalDevice,
                NumWrites,
                Writes,
                0,
                nullptr);
}

VkResult Core::VulkanManager::CreateComputePipelines(
        VkComputePipelineCreateInfo *Infos,
        const uint32_t Count,
        VkPipeline &OutPipelines)
{
    return vkCreateComputePipelines(
                m_VulkanContext.LogicalDevice,
                VK_NULL_HANDLE,
                Count,
                Infos,
                nullptr,
                &OutPipelines);
}

VkResult Core::VulkanManager::CreatePipelineLayout(
        VkPipelineLayoutCreateInfo &Info,
        VkPipelineLayout &OutLayout)
{
    return vkCreatePipelineLayout(
                m_VulkanContext.LogicalDevice,
                &Info,
                nullptr,
                &OutLayout);
}

VkResult Core::VulkanManager::CreateDescriptorPool(
        VkDescriptorPoolCreateInfo &Info,
        VkDescriptorPool &OutPool)
{
    return vkCreateDescriptorPool(
                m_VulkanContext.LogicalDevice,
                &Info,
                nullptr,
                &OutPool);
}

VkResult Core::VulkanManager::CreateDescriptorSetLayout(
        VkDescriptorSetLayoutCreateInfo &Info,
        VkDescriptorSetLayout &OutLayout)
{
    return vkCreateDescriptorSetLayout(
                m_VulkanContext.LogicalDevice,
                &Info,
                nullptr,
                &OutLayout);
}

VkResult Core::VulkanManager::AllocateDesctriptorSets(
        VkDescriptorSetAllocateInfo &Info,
        VkDescriptorSet &OutSets)
{
    return vkAllocateDescriptorSets(
                m_VulkanContext.LogicalDevice,
                &Info,
                &OutSets);
}

bool Core::VulkanManager::UpdateUBO(
        const UniformBufferObject *UBO)
{
    const VkDeviceSize bufferSize =
            sizeof(UniformBufferObject);
    if (!CopyDataToDeviceDirect(
                bufferSize,
                (void*)UBO,
                m_VulkanContext.UBOMem))
    {
        SDL_SetError(
                    "CopyDataToDeviceDirect(Vertex UBO) failed with:\n%s",
                    SDL_GetError());
        return false;
    }

    return true;
}
