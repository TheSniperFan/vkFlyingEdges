#pragma once

#include <type_traits>



/**
 * @brief Lerp linearlly interpolates a floating point number between two
 * values. Does not clamp the Time value.
 * @param From Value at Time = 0.
 * @param To Value at Time = 1.
 * @param Time not normalized time value.
 * @return Interpolated value.
 */
template<typename T>
static T Lerp(
        T From,
        T To,
        float Time)
{
    static_assert(
                std::is_same<T, float>::value ||
                std::is_same<T, double>::value,
                "Lerp only valid for floating point types!");

    return From + Time * (To - From);
}


/**
 * @brief Clamp clamps a numerical value between a minimum and a maximum.
 * @param Val Value to be clamped.
 * @param Min Lower limit.
 * @param Max Upper limit.
 * @return Clamped value.
 */
template<typename T>
static T Clamp(
        T Val,
        T Min,
        T Max)
{
    return (Val < Min) ? Min : (Val > Max) ? Max : Val;
}
