#pragma once

#include <string>
#include <chrono>
#include <vector>

#include "coreTypes.h"

using Main::InputOptions;


namespace Core
{
    class SDLManager;
    class VulkanManager;
    class VertexGen;
}
class Algorithm;

using namespace std::chrono;
class ApplicationCore
{
public:
    ApplicationCore(
            const InputOptions Options,
            bool &OutSuccess);
    ~ApplicationCore();

    /**
     * @brief Update Performs the update loop of the application.
     * @return Whether another update should occur or the application should
     * quit.
     */
    bool Update();
    /**
     * @brief GetError Returns the last error state of the system. Valid after
     * shutdown. Allows to find out why the application was shut down
     * after the fact.
     * @param OutError Optional to retrieve the error code which may come from
     * either SDL2 or Vulkan.
     * @return Human readable description.
     */
    std::string GetError(
            int *OutErrorCode = nullptr);

private:
    Algorithm *Algo;

    Core::SDLManager *SDLManager;
    Core::VulkanManager *VulkanManager;
    int ErrorCode;
    std::string ErrorString;

    /**
     * @brief RenderFrame renders and displays the content.
     * @return Success.
     */
    bool RenderFrame();
    /**
     * @brief UpdateDeltaTime updates the DeltaTime variable with the new
     * value. Should only be called once at the beginning of Update().
     */
    void UpdateDeltaTime();
    /**
     * @brief UpdateMVP updates the model-view-projection matrix used to
     * transform the scene.
     */
    void UpdateUBO();
    /**
     * @brief UpdateCamera updates the camera's rotation, position and FOV
     * based on the user's input.
     */
    void UpdateCamera();
    /**
     * @brief Contour runs the isocontouring algorithm.
     * @return False, if there was an error.
     */
    bool Contour();

    bool ReadInputData(
            const InputOptions Options);

    void CreateSphere(
            const int Dimensions[3]);
    void CreateCayleySurface(
            const int Dimensions[3]);
    template <typename T>
    void ReadFromFile(
            const InputOptions Options);

    // Camera state
    float CameraYaw;
    float CameraYawSpeed;
    float CameraPitch;
    float CameraPitchSpeed;
    float CurrentFOV;
    float TargetFOV;
    float CurrentThreshold;
    float LastThreshold;
    float ThresholdMin;
    float ThresholdMax;
    float TargetCorrectionFactor;
    float CurrentCorrectionFactor;
    uint32_t CurrentViewMode;
    uint8_t CurrentPipeline;
    float TargetYawSpeed;
    bool ChangedPipeline;
    bool ChangedAutoRotation;

    Core::UniformBufferObject UBO;

    /**
     * @brief DeltaTime is the time in seconds since the last frame
     * was rendered.
     */
    float DeltaTime;
    /**
     * @brief LastUpdateTime stores the time the last frame was rendered.
     */
    time_point<high_resolution_clock> LastUpdateTime;

    std::vector<float> InputData;
    std::vector<float> InputDataRaw;
};
