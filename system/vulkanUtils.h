#pragma once

#include <string>
#include <vulkan/vulkan.h>
#include "coreTypes.h"

namespace Core
{
    /**
     * @brief The VulkanManager class handles the initialization and cleanup of
     * the vulkan related data structures.
     */
    class VulkanManager
    {
    public:
        VulkanManager(
                const SDLStateInfo &SDLState,
                bool &OutSuccess,
                int &OutErrorCode,
                std::string &OutErrorString);
        ~VulkanManager();

        VulkanContextData &VulkanContext()
        {
            return m_VulkanContext;
        }

        VkCommandBuffer BeginSingleTimeCommands();
        void EndSingleTimeCommands(
                VkCommandBuffer CommandBuffer,
                VkQueue Queue);

    private:
        VulkanContextData m_VulkanContext;

        /**
         * @brief CreateInstance Creates a vulkan instance with the platform
         * specific extensions.
         * @param SDLState The SDL information used for creating the window.
         * @return Success.
         */
        bool CreateInstance(const SDLStateInfo &SDLState);
        /**
         * @brief CreateSurface Creates a vulkan surface with with the platform
         * specific extensions.
         * @param SDLState The SDL information used for creating the window.
         * @return Success.
         */
        bool CreateSurface(const SDLStateInfo &SDLState);
        bool CreateDevice();
        void CreateQueues();
        bool CreateSwapchain(const SDLStateInfo &SDLState);

        bool SupportsPresentMode(
                std::vector<VkPresentModeKHR> &InSupportedModes,
                VkPresentModeKHR Mode);
        void SetImageFormatAndColorSpace(
                std::vector<VkSurfaceFormatKHR> &InSupportedFormats,
                VkFormat &OutFormat,
                VkColorSpaceKHR &OutColorSpace);

        bool CreateSwapchainImageViews();
        bool CreateRenderPass();
        bool CreateGraphicsDescriptorSetLayout();
        bool CreateGraphicsPipeline();
        bool CreateFramebuffers();
        bool CreateCommandPool();
        bool CreateDepthResources();
        bool CreateGraphicsDescriptorPool();
        bool CreateGraphicsDescriptorSet();
        bool CreateVertexBuffer();
        bool CreateUniformBuffer();
        bool CreateCommandBuffers();
        bool CreateSemaphores();

        bool FindMemoryType(
                uint32_t TypeFilter,
                VkMemoryPropertyFlags Properties,
                uint32_t &OutType);
        bool FindSupportedFormat(
                const std::vector<VkFormat> &Canidates,
                VkImageTiling Tiling,
                VkFormatFeatureFlags Features,
                VkFormat &OutFormat);
        bool FindSupportedDepthFormat(
                VkFormat &OutFormat);
        bool HasStencilComponent(VkFormat Format);

        bool CreateImage(
                uint32_t Width,
                uint32_t Height,
                VkFormat Format,
                VkImageTiling Tiling,
                VkImageUsageFlags Usage,
                VkMemoryPropertyFlags Properties,
                VkImage &Image,
                VkDeviceMemory &ImageMemory);
        bool CreateImageView(VkImage Image,
                VkFormat Format,
                VkImageAspectFlags AspectFlags,
                VkImageView &OutView);
        bool TransitionImageLayout(
                VkImage Image,
                VkFormat Format,
                VkImageLayout OldLayout,
                VkImageLayout NewLayout);

    public:
        bool LoadShaderFromDisk(
                std::vector<char> &OutShader,
                const char *Path);
        bool CreateShaderModule(
                const std::vector<char> &InShader,
                VkShaderModule &OutModule);

        VkQueue GraphicsQueue() const
        {
            return m_VulkanContext.GraphicsQueue;
        }
        VkQueue ComputeQueue() const
        {
            return m_VulkanContext.ComputeQueue;
        }
        VkPhysicalDeviceLimits DeviceLimits() const
        {
            return m_VulkanContext.DeviceLimits;
        }

        bool CreateBuffer(
                VkDeviceSize Size,
                VkBufferUsageFlags Usage,
                VkMemoryPropertyFlags Properties,
                VkBuffer &Buffer,
                VkDeviceMemory &Memory);
        void CopyBuffer(
                VkBuffer SrcBuffer,
                VkBuffer DstBuffer,
                VkDeviceSize Size);
        bool CopyDataToDevice(
                VkDeviceSize BufferSize,
                void *SourceData,
                VkBufferUsageFlags Usage,
                VkBuffer &DeviceBuffer,
                VkDeviceMemory &DeviceMemory);
        bool CopyDataToDeviceDirect(
                VkDeviceSize BufferSize,
                void *Src,
                VkDeviceMemory Dst);
        bool CopyDataToHost(
                VkDeviceSize BufferSize,
                const VkBuffer SourceBuffer,
                void *&DstBuffer);
        bool CopyDataToHostDirect(
                VkDeviceSize BufferSize,
                const VkDeviceMemory Src,
                void *&Dst);

        void DestroyBuffer(
                VkBuffer Buffer);
        void FreeDeviceMemory(
                VkDeviceMemory Memory);

        bool UpdateRenderCommandBuffers(
                const VkBuffer *VertexBuffer,
                const int NumVerts);
        bool UpdateRenderCommandBuffers(
                const VkBuffer *VertexBuffer,
                const VkBuffer *IndexBuffer,
                const int NumTris);

        void DestroyDescriptorPool(
                VkDescriptorPool &Pool);
        void DestroyPipelineLayout(
                VkPipelineLayout &Layout);
        void DestroyDescriptorSetLayout(
                VkDescriptorSetLayout &Layout);
        void DestroyPipeline(
                VkPipeline &Pipeline);
        void DestroyShaderModule(
                VkShaderModule &Module);

        void UpdateDescriptorSets(
                const VkWriteDescriptorSet *Writes,
                const uint32_t NumWrites);

        VkResult CreateComputePipelines(
                VkComputePipelineCreateInfo *Infos,
                const uint32_t Count,
                VkPipeline &OutPipelines);
        VkResult CreatePipelineLayout(
                VkPipelineLayoutCreateInfo &Info,
                VkPipelineLayout &OutLayout);
        VkResult CreateDescriptorPool(
                VkDescriptorPoolCreateInfo &Info,
                VkDescriptorPool &OutPool);
        VkResult CreateDescriptorSetLayout(
                VkDescriptorSetLayoutCreateInfo &Info,
                VkDescriptorSetLayout &OutLayout);
        VkResult AllocateDesctriptorSets(
                VkDescriptorSetAllocateInfo &Info,
                VkDescriptorSet &OutSets);

        bool UpdateUBO(
                const UniformBufferObject *UBO);
    };
}
