#pragma once



/*
 * Abstract base class for isocontouring algorithms.
 */
class Algorithm
{

public:

    /**
     * @brief ~Algorithm Virtual dtor.
     */
    virtual ~Algorithm() {}

    /**
     * @brief Init is called after the ctor. It can be thought of
     * as a ctor that may fail.
     * @return Success.
     */
    virtual bool Init() = 0;
    /**
     * @brief Run executes the entire algorithm for a given iso value.
     * @param Iso The current iso value.
     * @return Success.
     */
    virtual bool Run(
            const float Iso) = 0;

    /**
     * @brief NumVertices Returns the number of vertices generated the last
     * time the algorithm was ran.
     * @return Number of generated vertices.
     */
    virtual int NumVertices() const = 0;

    /**
     * @brief NumTriangles Returns the number of triangles generated the last
     * time the algorithm was ran.
     * @return The number of triangles generated.
     */
    virtual int NumTriangles() const = 0;

};
