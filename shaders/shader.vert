#version 450

layout(binding = 0) uniform UniformBufferObjectLayout
{
    mat4 Model;
    mat4 ViewProjection;
    float CorrectionFactor;
    uint ViewMode;
};

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inNormal;

layout(location = 0) out vec3 vNorm;
layout(location = 1) out vec3 vPos;

const vec3 eye = vec3(5.0f, 0.0f, 0.0f);

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    vec4 correctedPosition = inPosition;
    correctedPosition.z *= CorrectionFactor;

    vec4 worldPosition = Model * correctedPosition;
    vPos = eye - worldPosition.xyz;
    gl_Position = ViewProjection * worldPosition;

    switch (ViewMode)
    {
    default:
    case 1:
    case 2:
        vNorm = (Model * inNormal).xyz;
        return;
    case 3:
        vNorm = inNormal.xyz;
    case 4:
        return;
    }
}
