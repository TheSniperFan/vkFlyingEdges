#version 450

// Flying Edges algorithm
// Parallel implementation based on the original implementation
// in the vtk library:
// https://gitlab.kitware.com/vtk/vtk/blob/master/Filters/Core/vtkFlyingEdges3D.cxx

// Pass 3
// Calculates the total number of points to generate and the offsets used
// to segment the buffers pass 4 writes into.
// This is done sequentially on the GPU with one thread for simplicity. It
// could be implemented in parallel using a parallel prefix sum operation.

layout (
    local_size_x=1,
    local_size_y=1,
    local_size_z=1) in;


// UBO storing misc data that is useful throughout the execution
// of the algorithm.
layout(binding=0) uniform UniformBufferObjectLayout
{
    // The dimensions of the input data.
    ivec3 Dimensions;
};


// Edge meta data structure
// One tuple is stored per x edge.
struct EdgeMetaData
{
    int XIntersections;
    int YIntersections;
    int ZIntersections;
    int TriangleCount;
    int TrimStart;
    int TrimEnd;
};

// Stores one edge meta data tuple per x edge.
// Amount: Y * Z
layout(std430, binding=1) buffer EdgeMetaDataBufferLayout
{
    EdgeMetaData EdgeMeta[];
};


// Results from this pass.
layout(std430, binding=2) buffer SizeBufferLayout
{
    // Total number of vertices on the x edges
    int TotalX;
    // Total number of vertices on the y edges
    int TotalY;
    // Total number of vertices on the z edges
    int TotalZ;
    // Total number of triangles
    int TotalTris;
};



void main()
{
    int xCount = 0;
    int yCount = 0;
    int zCount = 0;
    int triCount = 0;

    const int yDim = Dimensions.y;
    const int zDim = Dimensions.z;

    for (int i = 0; i < zDim; ++i)
    {
        const int zInc =
                i * yDim;
        for (int ii = 0 ; ii < yDim; ++ii)
        {
            const int metaIdx = (zInc + ii);

            const int xPts = EdgeMeta[metaIdx].XIntersections;
            const int yPts = EdgeMeta[metaIdx].YIntersections;
            const int zPts = EdgeMeta[metaIdx].ZIntersections;
            const int tris = EdgeMeta[metaIdx].TriangleCount;

            const int newX = xCount + yCount + zCount;
            const int newY = newX + xPts;
            const int newZ = newY + yPts;

            EdgeMeta[metaIdx].XIntersections = newX;
            EdgeMeta[metaIdx].YIntersections = newY;
            EdgeMeta[metaIdx].ZIntersections = newZ;
            EdgeMeta[metaIdx].TriangleCount = triCount;

            xCount += xPts;
            yCount += yPts;
            zCount += zPts;
            triCount += tris;

        }
    }

    TotalX = xCount;
    TotalY = yCount;
    TotalZ = zCount;
    TotalTris = triCount;
}
