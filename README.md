# GPU Flying Edges implementation using Vulkan

Flying Edges based on: https://gitlab.kitware.com/vtk/vtk/blob/master/Filters/Core/vtkFlyingEdges3D.cxx

Marching Cubes based on: http://paulbourke.net/geometry/polygonise/


## Description:

This project implements the Flying Edges isocontouring algorithm as a GPGPU
application using the Vulkan API. Flying Edges is a more modern alternative to
the old Marching Cubes algorithm, that seeks to improve efficiency and solve the
problem of running into memory bottlenecks. FE generates an indexed mesh that
contains no redundant vertices and does so without a dedicated point-merging
pass (e.g. in a geometry shader).

While the algorithm was designed for use on many-core architectures with high-
performance cores (i.e. servers and workstations), it still performs quite well
on GPUs, despite their low-performance cores.

An implementation of the Marching Cubes algorithm was added for comparison. The
application uses platform-independent technologies and was tested on
**GNU/Linux** and **Windows 10**.

## Compilation:

This project was developed using:

* CMake (3.1)
* SDL2 (2.0.7)
* glm (0.9.8.5)
* Vulkan (1.0.65)

On Windows (and Linux, if SDL2 and glm weren't installed using the package 
manager), the following environment variables have to be specified:

Variable | Value
-------- | -----
SDL2DIR | Path to the downloaded SDL2 library
GLMDIR | Path to the downloaded glm library

### Produced Binaries

Binary | Description
-------- | -----
vkFlyingEdges | Normal GPU implementation of Flying Edges
vkMarchingCubes | Normal GPU implementation of Marching Cubes
vkFlyingEdges_TESTMODE | Refreshes the mesh even if the isovalue didn't change
vkMarchingCubes_TESTMODE | Refreshes the mesh even if the isovalue didn't change

## Controls:

Key | Description
-------- | -----
Left mouse drag | Rotate object
Right mouse drag | Zoom
Mouse wheel | Adjust isovalue
Left shift + mouse wheel | Fine-adjust isovalue
Left ctrl + mouse wheel | Adjust vertical scaling factor
R | Toggle autorotation
1 | Blinn-Phong shading
2 | Visualize view-space normals
3 | Visualize world-space normals
4 | Green flat shading
W | Toggle wireframe\*

\* May significantly affect performance on GeForce cards with Marching Cubes

## Command line arguments:

### Misc:

Syntax | Description
-------- | -----
app -h/--help | Show help

### Synthetic Data:

Syntax | Description
-------- | -----
app | Mode 0 (sphere) (**implicit**), Resolution 96x96x96 (**implicit**)
app 0 | Mode 0 (sphere) (**explicit**), Resolution 96x96x96 (**implicit**)
app 0 256 125 113 | Mode 0 (sphere) (**explicit**), Resolution 256x125x113 (**explicit**)
app 1  | Mode 1 (cayley) (**explicit**), Resolution 96x96x96 (**implicit**)
app 1 256 125 113 | Mode 1 (cayley) (**explicit**), Resolution 256x125x113 (**explicit**)

### Raw Data:

Syntax | Description
-------- | -----
app 2 *FILE FORMAT DIMENSIONS* [flip] | General syntax
*FILE* | Path to the raw file
*FORMAT* | Format code (see below)
*DIMENSIONS* | Size of the data (e.g. 96 96 48)
flip | Optional, reverses byteorder if set

#### Format Codes:

Code | Description
-------- | -----
f16, f32, f64 | Floating point, [16, 32, 64] Bit wide
u8, u16, u32, u64 | Unsigned integer, [8, 16, 32, 64] Bit wide
s8, s16, s32, s64 | Signed integer, [8, 16, 32, 64] Bit wide
